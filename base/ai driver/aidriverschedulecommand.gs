include "aiprioritysettingsbehavior.gs"
include "driverschedulecommand.gs"
include "aidriver.gs"

final class AIDriverCustomCommand isclass CustomCommand
{

  AIDriverTargetBase _target;
  int _markerside;
  
  public AIDriverCustomCommand Init(AIDriverTargetBase target, int markerside)
  {
    _target = target;
    _markerside = markerside; 
    return me;
  } 

  public bool Execute(Train train, int px, int py, int pz)
  {
    AIDriver driver = cast<AIDriver>new AIDriver().Init(train);
    driver.Start(_markerside, _target);
    return true;
  }

  public bool ShouldStopTrainOnCompletion() { return false; }

};

final class AIDriverScheduleCommand isclass DriverScheduleCommand
{

  string _stationname;          //Имя станции для стоп маркера
  int _action;                  //Простое движение
  int _stopmarkertype;          //Тип требуемого стоп маркера
  int _sections;                //Количество секций для стоп-маркера
  int _markerside;              //Сторона, относительно маркера, где нужно остановиться
  TrackMark _marker;            //Маркер, до которого нужно ехать

  public bool BeginExecute(DriverCharacter driver)
	{
		Train train = driver.GetTrain();
		if (!train) return false;
    AIDriverTargetBase target;
    int markerside = AIDriver.TARGETMOVE_TO;
    if (_action == 2) {
      AIDriverTargetTrackMark newtarget = new AIDriverTargetTrackMark();
      newtarget.SetTrackMark(_marker);
      target = newtarget;
      markerside = _markerside;  
    } else if (_action) {
      AIDriverTargetStopMarker newtarget = new AIDriverTargetStopMarker();
      int markertype = _stopmarkertype - 1; 
      newtarget.SetStation(_stationname);
      newtarget.SetMarkerType(markertype);
      if (markertype == AIStopMarkerBase.TYPE_LOCOMOTIVE or
      markertype == AIStopMarkerBase.TYPE_CONSIST)
        newtarget.SetSectionCount(_sections);
      target = newtarget;
    }
    AIDriverCustomCommand command = new AIDriverCustomCommand().Init(target, markerside);
		driver.DriverCustomCommand(command);
		driver.DriverIssueSchedule();
		return true;
	}

	public object GetIcon(void)
  {
    return (object)(GetDriverCommand());
  }
  
  public string GetTooltip(void) 
  {
		StringTable strtable = GetAsset().GetStringTable();
    if (_marker and !Router.DoesGameObjectStillExist(_marker)) _marker = null;
    if (_action == 2 and _marker) {
      if (_markerside == 2) return strtable.GetString1("tooltip-drivevia", _marker.GetLocalisedName());
      else if (_markerside == 1) return strtable.GetString1("tooltip-drivefor", _marker.GetLocalisedName());
      else if (_markerside == 0) return strtable.GetString1("tooltip-driveto", _marker.GetLocalisedName());
    } else if (_action == 1) {
      string tooltip;
      if (_stopmarkertype == 4 and _sections > 0) tooltip = strtable.GetString1("tooltip-stopmarker-stoptrain-section", _sections);
      else if (_stopmarkertype == 4 and _sections == 0)  tooltip = strtable.GetString("tooltip-stopmarker-stoptrain");
      else if (_stopmarkertype == 3 and _sections > 0)  tooltip = strtable.GetString1("tooltip-stopmarker-stoploco-section", _sections);
      else if (_stopmarkertype == 3 and _sections == 0)  tooltip = strtable.GetString("tooltip-stopmarker-stoploco");
      else if (_stopmarkertype == 2)  tooltip = strtable.GetString("tooltip-stopmarker-firstwagon");
      else if (_stopmarkertype == 1)  tooltip = strtable.GetString("tooltip-stopmarker-none");
      else if (_stopmarkertype == 0)  tooltip = strtable.GetString("tooltip-stopmarker-any");
      if (tooltip) {
        if (_stationname and _stationname.size()) tooltip = tooltip + " " + strtable.GetString1("tooltip-station", _stationname);
        else tooltip = tooltip + ".";
        return tooltip;   
      }
    } else if (_action == 0) return strtable.GetString("tooltip-drive");
    return strtable.GetString("tooltip-unsettings");
  }

  public bool IsReadyToExecute(DriverCharacter driver) 
  { 
    return _action == 0 or (_action == 1 and _stopmarkertype >= 0 and _stopmarkertype <= 4 and ((_stopmarkertype != 3 and _stopmarkertype != 4) or _sections > 0)) or 
           (_marker and _markerside >= 0 and _markerside <= 2);
  }

  Soup GetUserInterfaceProperties()
  {
    StringTable strtable = GetAsset().GetStringTable();
    Soup settingssoup = inherited();
    Soup valuessoup = Constructors.NewSoup();
    Soup actionsoup = Constructors.NewSoup();
    if (_action == 2) actionsoup.SetNamedTag("value", strtable.GetString("value-action-marker"));
    else if (_action == 1) actionsoup.SetNamedTag("value", strtable.GetString("value-action-stopmarker"));
    else if (_action == 0) actionsoup.SetNamedTag("value", strtable.GetString("value-action-drive"));     
    valuessoup.SetNamedTag("0", strtable.GetString("value-action-drive"));      
    valuessoup.SetNamedTag("1", strtable.GetString("value-action-stopmarker"));      
    valuessoup.SetNamedTag("2", strtable.GetString("value-action-marker"));      
    actionsoup.SetNamedTag("name", strtable.GetString("settings-action")); 
    actionsoup.SetNamedTag("type", "list");
    actionsoup.SetNamedSoup("list-options", valuessoup);
    settingssoup.SetNamedSoup("action", actionsoup);
    if (_action == 1) {
      AICore core = cast<AICore>TrainzScript.GetLibrary(GetAsset().LookupKUIDTable("ai-driver-core"));
      string stationname = _stationname;
      if (!stationname or !stationname.size()) stationname = strtable.GetString("value-station-any");
      Soup stationsoup = Constructors.NewSoup();
      Soup stationvaluessoup = Constructors.NewSoup();
      string[] stationlist = core.StationNameList();
      int i, count = stationlist.size(); 
      stationvaluessoup.SetNamedTag("0", strtable.GetString("value-station-any"));
      for (i = 0; i < count; ++i)
        stationvaluessoup.SetNamedTag(i + 1, stationlist[i]);
      stationsoup.SetNamedTag("name", strtable.GetString("settings-station")); 
      stationsoup.SetNamedTag("value", stationname); 
      stationsoup.SetNamedTag("type", "list");
      stationsoup.SetNamedSoup("list-options", stationvaluessoup);
      settingssoup.SetNamedSoup("stations", stationsoup);
      Soup markersoup = Constructors.NewSoup();
      Soup markervaluessoup = Constructors.NewSoup();
      for (i = 0; i <= 4; ++i)
        markervaluessoup.SetNamedTag(i, strtable.GetString("value-stopmarker-" + i));
      if (_stopmarkertype >= 0) markersoup.SetNamedTag("value", strtable.GetString("value-stopmarker-" + _stopmarkertype));
      markersoup.SetNamedTag("name", strtable.GetString("settings-stopmarker")); 
      markersoup.SetNamedTag("type", "list");
      markersoup.SetNamedSoup("list-options", markervaluessoup);
      settingssoup.SetNamedSoup("stopmarker", markersoup);
      if (_stopmarkertype == 3 or _stopmarkertype == 4) {
        int count = 8;
        if (_stopmarkertype == 3) count = 4;
        Soup sectionsoup = Constructors.NewSoup();
        Soup sectionvaluessoup = Constructors.NewSoup();
        sectionvaluessoup.SetNamedTag("0", strtable.GetString("value-section-any"));
        for (i = 1; i <= count; ++i) 
          sectionvaluessoup.SetNamedTag(i, strtable.GetString1("value-section", i));
        if(_sections > 0) sectionsoup.SetNamedTag("value", strtable.GetString1("value-section", _sections));
        else if(_sections == 0) sectionsoup.SetNamedTag("value", strtable.GetString("value-section-any"));
        sectionsoup.SetNamedTag("name", strtable.GetString("settings-section")); 
        sectionsoup.SetNamedTag("type", "list");
        sectionsoup.SetNamedSoup("list-options", sectionvaluessoup);
        settingssoup.SetNamedSoup("section", sectionsoup);
      }
    } else if (_action == 2) {
      Soup markersoup = Constructors.NewSoup();
      if (_marker and !Router.DoesGameObjectStillExist(_marker)) _marker = null;
      if(_marker) markersoup.SetNamedTag("value-display-name", _marker.GetLocalisedName()); 
      markersoup.SetNamedTag("name", strtable.GetString("settings-marker")); 
      markersoup.SetNamedTag("type", "map-object," + AssetCategory.Trackmark);
      settingssoup.SetNamedSoup("marker", markersoup);
      if (_marker) {
        int i;
        Soup sidesoup = Constructors.NewSoup();
        Soup sidevaluessoup = Constructors.NewSoup();
        for (i = 0; i < 3; ++i)
          sidevaluessoup.SetNamedTag(i, strtable.GetString("value-side-" + i));
        if (_markerside >= 0) sidesoup.SetNamedTag("value", strtable.GetString("value-side-" + _markerside));
        sidesoup.SetNamedTag("name", strtable.GetString("settings-side")); 
        sidesoup.SetNamedTag("type", "list");
        sidesoup.SetNamedSoup("list-options", sidevaluessoup);
        settingssoup.SetNamedSoup("side", sidesoup);
      }
    }
    return settingssoup;
  }

  bool SetUserInterfaceProperty(string propertyID, Soup propertyData)
  {
    if (propertyID == "action"){
      _action = propertyData.GetNamedTagAsInt("list-index", -1);
      _marker = null;
    } else if (propertyID == "stopmarker") {
      _stopmarkertype = propertyData.GetNamedTagAsInt("list-index", -1);
      _sections = -1;
    } else if (propertyID == "marker") {
      _marker = cast<TrackMark>Router.GetGameObject(propertyData.GetNamedTagAsGameObjectID("value"));
      _markerside = -1;
    } else if (propertyID == "stations") {
      if (propertyData.GetNamedTagAsInt("list-index", -1) > 0) _stationname = propertyData.GetNamedTag("value");
      else _stationname = "";  
    } 
    else if (propertyID == "section") _sections = propertyData.GetNamedTagAsInt("list-index", -1);
    else if (propertyID == "side") _markerside = propertyData.GetNamedTagAsInt("list-index", -1);
    return true;  
  }

  public Soup GetProperties(void) 
  { 
    if (_marker and !Router.DoesGameObjectStillExist(_marker)) _marker = null;
    Soup soup = inherited();
    soup.SetNamedTag("Action", _action);
    if (_action == 1) {
      soup.SetNamedTag("StationName", _stationname);
      soup.SetNamedTag("StopMarkerType", _stopmarkertype);
      if (_stopmarkertype == 3 or _stopmarkertype == 4)
        soup.SetNamedTag("SectionCount", _sections);
    } else if (_action == 2) {
      if (_marker) soup.SetNamedTag("MarkerId", _marker.GetGameObjectID());
      soup.SetNamedTag("MarkerSide", _markerside);
    }
    return soup; 
  }

  public void SetProperties(Soup soup) 
  {
    _stationname = soup.GetNamedTag("StationName");
    _action = soup.GetNamedTagAsInt("Action", -1);
    _stopmarkertype = soup.GetNamedTagAsInt("StopMarkerType", -1);
    _sections = soup.GetNamedTagAsInt("SectionCount", -1);
    _markerside = soup.GetNamedTagAsInt("MarkerSide", -1);
    GameObjectID markerid = soup.GetNamedTagAsGameObjectID("MarkerId");
    if (_action == 2 and markerid) _marker = cast<TrackMark>Router.GetGameObject(markerid);
  }
  
  

};