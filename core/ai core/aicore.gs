//=============================================================================
// File: aicore.gs
// Desc: Набор классов для реализации ядра авто-машиниста 
// Auth: Алексей 'Эрендир' Зверев 2020 © Trainz Dev Team
//       Licence: GNU GPL
//=============================================================================

include "aiprioritysettingsbehavior.gs"
include "library.gs"


//=============================================================================
// Name: AIStationNameAction
// Desc: Вспомогательный класс для элемента конвеера управления списком
//       станций.
//=============================================================================
class AIStationNameAction isclass GSObject
{
  
  //=============================================================================
  // Name: stationName
  // Desc: Имя станции, с которым выполняется операция
  //=============================================================================
  public string stationName;
  
  //=============================================================================
  // Name: removing
  // Desc: Значение, если имя станции должно быть удалено; в противном случае —
  //       значение false, если имя станции должно быть добавлено.
  //=============================================================================
  public bool removing; 
  
  //=============================================================================
  // Name: prevItem
  // Desc: Ссылка на предыдущий элемент связаного списка
  //=============================================================================
  public AIStationNameAction prevItem;
  
  //=============================================================================
  // Name: prevItem
  // Desc: Ссылка на следующий элемент связаного списка
  //=============================================================================
  public AIStationNameAction nextItem;

};


//=============================================================================
// Name: AIStationNameAction
// Desc: Класс ядра автомашиниста
//=============================================================================
final class AICore isclass Library
{

  //=============================================================================
  // Name: NewId
  // Desc: Генерирует новый уникальный идентификатор.
  // Retn: Уникальный идентификатор (значение >= 0) 
  //=============================================================================
  public int NewId(void);
  
  //=============================================================================
  // Name: GetPrioritySettingsBehavior
  // Desc: Возвращает ссылку на правило редактирования парметров приоритетов.
  // Retn: Объект AIPrioritySettingsBehavior, представляющий правило 
  //       редактирования параметрво приоритетов; или значение null, если правило
  //       не установлено в сесии. 
  //=============================================================================
  public AIPrioritySettingsBehavior GetPrioritySettingsBehavior();
  
  //=============================================================================
  // Name: GetPrioritySettingsBehavior
  // Desc: Устанавливает активное правило редактирования параметров приоритетов.
  // Parm: prioritySettings – объект AIPrioritySettingsBehavior, представляющий 
  //       правило редактирования параметрво приоритетов. Значеие null не 
  //       допускается.
  // Parm: force — значение true, если нужнго назначить указаное правило даже
  //       если имеется другое назначение правило; в противном случае —
  //       хначение false.        
  // Retn: Значение true, если правило prioritySettings было удачно назначено 
  //       активным; в противном случае — значение false.
  //=============================================================================
  public bool SetPrioritySettingsBehavior(AIPrioritySettingsBehavior prioritySettings, bool force);  

  //=============================================================================
  // Name: SetClipboardData
  // Desc: Сохранение данных в буфер обмена.
  // Parm: key — ключ, под которым будут храниться данные в буфере обмена. 
  // Parm: data — Объект Soup, содержащий данные
  //=============================================================================
  public final void SetClipboardData(string key, Soup data);

  //=============================================================================
  // Name: GetClipboardData
  // Desc: Получает данные из буфера обмена.
  // Parm: key — ключ, под которым хранятся данные в буфере обмена. 
  // Retn: Объект Soup, содержащий данные; или значение null, если данные с 
  //       ключём key не существуют. 
  //=============================================================================
  public final Soup GetClipboardData(string key);

  //=============================================================================
  // Name: ContainsDataInClipboard
  // Desc: Проверяет наличие данных в буфере обмена.
  // Parm: key — ключ, наичие данных которого требуется проверить. 
  // Retn: Значение true, если данные с ключём key присутствуют в буфере рбмена;
  //       в противном случае — значение false. 
  //=============================================================================
  public final bool ContainsDataInClipboard(string key);
  
  //=============================================================================
  // Name: AddStationName
  // Desc: Добавляет имя станции в список, если такого имени там ещё нет.
  // Parm: statonName — добавляемое наименование станции 
  // Note: Изменени списка происходит ассинхронно, вне этой функции. На момент
  //       выхода из функции список ещё не будет изменён. Изменени списка станций
  //       можно отселживать по событию "AIDriver", "StationListChanged"
  //=============================================================================
  public final void AddStationName(string statonName);
  
  //=============================================================================
  // Name: TryRemoveStationName
  // Desc: Пытается удалить имя станции из списка, если это возможно.
  // Parm: statonName — удаляемое наименование станции 
  // Note: Изменени списка происходит ассинхронно, вне этой функции. На момент
  //       выхода из функции список ещё не будет изменён. Изменени списка станций
  //       можно отселживать по событию "AIDriver", "StationListChanged"
  //=============================================================================
  public final void TryRemoveStationName(string statonName);
  
  //=============================================================================
  // Name: StationNameList
  // Desc: Вовзвращает списко наименоаний станции
  // Retn: Массив строк, содержащий наименования станций.
  //=============================================================================
  public final string[] StationNameList();
  
  

	//
	// РЕАЛИЗАЦИЯ
	//
  



  int _currentid;
  
  AIPrioritySettingsBehavior _prioritysettings;
  
  Soup _clipboard;
  
  int _state;                                                 //Хранит различне состяония ядра


  AIStationNameAction _firststationnameactionitem;
  AIStationNameAction _laststationnameactionitem;
  string[] _stationnamelist;
  int[] _stationnamecounterlist;



  define int STATE_STATIONNAMECONVEYOR      = (1 << 31);



  public int NewId(void)
  {
    return _currentid++; 
  }

  public AIPrioritySettingsBehavior GetPrioritySettingsBehavior()
  {
    if(_prioritysettings and !Router.DoesGameObjectStillExist(_prioritysettings)) _prioritysettings = null;
    return _prioritysettings;
  }

  public bool SetPrioritySettingsBehavior(AIPrioritySettingsBehavior prioritySettings, bool force)
  {
    if(!prioritySettings) { Exception("AICore.SetPrioritySettingsBehavior> Значение аргумента prioritySettings не может быть null."); return false; }
    if(!Router.DoesGameObjectStillExist(prioritySettings)) { Exception("AICore.SetPrioritySettingsBehavior> Значение аргумента prioritySettings содержит недействительный объект."); return false; }
    if(force or !_prioritysettings or !Router.DoesGameObjectStillExist(_prioritysettings)) {
      _prioritysettings = prioritySettings;
      PostMessage(me, "AIDriver", "PriorityChanged", 0.0);
      return true;
    }
    return false;
  }  









  final string GetValidClipboardKeyValue(string key)
  {
    if(!key or !key.size()) return (string)null;
    Str.TrimLeft(key, " ");
    Str.TrimRight(key, " ");
    return key;
  }

  //Добавление данных в буфер обмена
  public final void SetClipboardData(string key, Soup data)
  {
    key = GetValidClipboardKeyValue(key);
    if(!key or !key.size()) { 
      Exception("AICore.SetClipboardData> Не задан ключ данных буфера обмена"); 
      return; 
    }
    if (!data) { 
      Exception("AICore.SetClipboardData> Значение аргумента data не может быть null.");
      return;
    }
    if (!_clipboard) _clipboard = Constructors.NewSoup(); 
    _clipboard.SetNamedSoup(key, data); //Сохранение данных в суп буфера обмена
  }

  public final Soup GetClipboardData(string key)
  {
    key = GetValidClipboardKeyValue(key);
    if(!key or !key.size()) { 
      Exception("AICore.SetClipboardData> Не задан ключ данных буфера обмена"); 
      return null; 
    }
    if (!_clipboard or _clipboard.GetIndexForNamedTag(key) < 0) return null;
    return _clipboard.GetNamedSoup(key);
  }

  public final bool ContainsDataInClipboard(string key)
  {
    key = GetValidClipboardKeyValue(key);
    if(!key or !key.size()) { 
      Exception("AICore.SetClipboardData> Не задан ключ данных буфера обмена"); 
      return false; 
    }
    return _clipboard  and _clipboard.GetIndexForNamedTag(key) >= 0;
  }





  //========================СПИСОК ИМЁН СТАНЦИЙ======================
  
  //Добавляет новую запись действия над списком имён станциий в конвейр
  //stationName — наименованеи станции
  //removing — значение true, если удаление, значение false, если добавление
  final AIStationNameAction PushStationNameActionItem(string stationName, bool removing)
  {
    AIStationNameAction item = new AIStationNameAction();
    item.stationName = stationName;
    item.removing = removing;
    if (!_firststationnameactionitem) _firststationnameactionitem = item;
    if (_laststationnameactionitem) {
      _laststationnameactionitem.nextItem = item;
      item.prevItem = _laststationnameactionitem; 
    }
    _laststationnameactionitem = item;
    return item;
  }
  
  //Возвращает первую запись о действии над списком имён станий из конвейра
  final AIStationNameAction PullStationNameActionItem(void)
  {
    if (_firststationnameactionitem) {
      AIStationNameAction returnitem = _firststationnameactionitem; 
      if(_firststationnameactionitem.nextItem)
        _firststationnameactionitem.nextItem.prevItem = null;
      _firststationnameactionitem = _firststationnameactionitem.nextItem;
      if (!_firststationnameactionitem) _laststationnameactionitem = null;
      return returnitem;
    }
    return null;
  }
  
  final int ResizeStationNameList(int insertIndex, int sleepCounter)
  {
    int i;
    for (i = _stationnamelist.size() - 1; i >= insertIndex; --i) {
      _stationnamelist[i + 1] = _stationnamelist[i];
      _stationnamecounterlist[i + 1] = _stationnamecounterlist[i];
      if (++sleepCounter >= 1000) {
        Sleep(0.001);
        sleepCounter = 0;
      }      
    }
    _stationnamelist[insertIndex] = null;
    _stationnamecounterlist[insertIndex] = 0;
    return sleepCounter;
  }
  
  final int FindStationNameListIndex(string stationName, bool insert)
  {
    if (!_stationnamelist) {
      _stationnamelist = new string[0];
      _stationnamecounterlist = new int[0];
    }
    int rangeindex = 0;
    int range = _stationnamelist.size();
    while (true) {
      int index = rangeindex + range / 2;
      int lowerrange = index - rangeindex;
      int upperrange = (rangeindex + range) - index - 1;
      if (index < _stationnamelist.size() and _stationnamelist[index] == stationName) return index;
      else if (index >= _stationnamelist.size() or (stationName < _stationnamelist[index] and lowerrange <= 0)) {
        if (insert) return index;
        break;
      }
      if (stationName < _stationnamelist[index]) range = lowerrange;
      else {
        range = upperrange;
        rangeindex = index + 1;
      }
    }
    return -1;
  }
  
  final thread void StationNameListConveyor()
  {
    while (true) {
      wait () {
        on "AIDriver", "StationNameConveyorItemAdded":
          break;
      }
      int sleepcounter = 0;
      _state = _state | STATE_STATIONNAMECONVEYOR;
      AIStationNameAction item = PullStationNameActionItem();
      bool listchanged = false;
      while (item) {
        int index = FindStationNameListIndex(item.stationName, !item.removing);
        if (!item.removing) {
          if(index == _stationnamelist.size() or _stationnamelist[index] != item.stationName) {
            sleepcounter = ResizeStationNameList(index, sleepcounter);
            _stationnamelist[index] = item.stationName;
            listchanged = true;
          }            
          ++_stationnamecounterlist[index];        
        } else if (index >= 0) {
          if (--_stationnamecounterlist[index] <= 0) {
            _stationnamelist[index, index + 1] = null;
            _stationnamecounterlist[index, index + 1] = null;
            listchanged = true;
          }
        }
        if (++sleepcounter >= 1000) {
          Sleep(0.001);
          sleepcounter = 0;
        }
        item = PullStationNameActionItem();
      } 
      _state = _state & ~STATE_STATIONNAMECONVEYOR;
      if (listchanged) PostMessage(me, "AIDriver", "StationListChanged", 0.0);
    }
  }

  public final void AddStationName(string statonName)
  {
    if (statonName) {
      Str.TrimLeft(statonName, " ");
      Str.TrimRight(statonName, " ");
      PushStationNameActionItem(statonName, false);
      if (!(_state & STATE_STATIONNAMECONVEYOR))
        PostMessage(me, "AIDriver", "StationNameConveyorItemAdded", 0.0);    
    }
  }
  
  public final void TryRemoveStationName(string statonName)
  {
    if (statonName) {
      Str.TrimLeft(statonName, " ");
      Str.TrimRight(statonName, " ");
      PushStationNameActionItem(statonName, true);    
      if (!(_state & STATE_STATIONNAMECONVEYOR))
        PostMessage(me, "AIDriver", "StationNameConveyorItemAdded", 0.0);    
    }
  }
  
  public final string[] StationNameList()
  {
    if (_stationnamelist) return _stationnamelist;
    return new string[0]; 
  }
  

  
  

  public final void Init(Asset asset)
  {
    inherited(asset);
    StationNameListConveyor();
  }  



};