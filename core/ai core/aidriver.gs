include "aidrivertarget.gs"
include "aidriverbase.gs"
include "ecl_bitlogic.gs"


class AIDriver isclass AIDriverBase
{
  

  public define int TARGETMOVE_TO = 0;
  public define int TARGETMOVE_FOR = 1;
  public define int TARGETMOVE_VIA = 2;
  
  public define int RESULT_ALREADYSTARTED   = 0;
  public define int RESULT_SUCCESS          = 1;
  public define int RESULT_TARGETCOMPLETE   = 2;
  public define int RESULT_BREAK            = 3;

  define int STATE_TARGETLOCK               = (1 << 0);    
  define int STATE_TARGETFOLLOWED           = (1 << 1);    

  define int STATE_BREAK                    = (1 << 30);
  define int STATE_WORKED                   = (1 << 31);

  AIDriverTargetBase _target;  

  float _targetstopoffset = 1.0;   //��������� �� �������� �������, �� ������� ����� ������������
  
  int _state;                      //������ ���������� ���������
  




  public mandatory AIDriverBase Init(Train train)
  {
    Sniff(train, "Schedule", "Abort", true);
    AddHandler(me, "Schedule", "Abort", "AbortHandler");
    return inherited (train);
  }


  final float GetTargetDistanceFromLastVehicle(bool seacrhBackward, float searchDistance)
  {
    if (!_target) return -1.0; 
    GSTrackSearch searcher;
    Vehicle[] vehicles = _train.GetVehicles();
    Vehicle lastvehicle = vehicles[vehicles.size() - 1];
    if (seacrhBackward) searcher = lastvehicle.BeginTrackSearch(!lastvehicle.GetDirectionRelativeToTrain());
    else searcher = lastvehicle.BeginTrackSearch(lastvehicle.GetDirectionRelativeToTrain()); 
		MapObject foundobject = searcher.SearchNext();
		float distance = searcher.GetDistance();
    searchDistance = searchDistance + lastvehicle.GetLength();
    while (foundobject and distance < searchDistance) {
      Trackside trackside = (cast<Trackside>foundobject);
      if (trackside and _target.IsTargetObject(trackside, seacrhBackward != searcher.GetFacingRelativeToSearchDirection(), distance)) return distance;
      foundobject = searcher.SearchNext();
			distance = searcher.GetDistance();
    }
    return -1.0;
  }




  //������������ �������� �� �������
  final int DriveTo(Vehicle vehicle, GSTrackSearch searcher)
  {
    //if(!me.ignoreSignal and me.CheckStopAfterDrivingOfRedSignals()) return false; //���� ���� �������� �������, � ����� ��������� �������, �� �������� �������.

		//���� ���������� ��������� ����������� ��������, �� �������� ������� ����������
    if (!_maxspeedisconstant) {
      float advisorylimit = _train.GetAdvisoryLimit();
      if (/*me.supportAdvisoryLimit  and */advisorylimit > 0)
			  _maxspeed = Math.Fmin(advisorylimit,  _train.GetSpeedLimit());
      else _maxspeed = _train.GetSpeedLimit();
    }

    Trackside currenttarget;
		MapObject foundobject = searcher.SearchNext();
    float distance = searcher.GetDistance();
		float vehlenght = vehicle.GetLength();

		float nextspeedboard = -1; //��������� ����������� �������� (���� ���� ��������)
		float nextspeedboarddist = -1; //��������� �� ���������� ����������� ��������
    float destinationdist = -1; //��������� �� ����
    float redsignaldist = -1; //��������� �� �������� ������
    float vehicledist = -1; //��������� �� ������ ���������� �������
    float vehicledestlength = -1; //����� ��������� �������, � ������� ����������� ��������
		
    
		int maxsearchdist = 5000; // me.GetMaxObjectSearchDist(prop, 100);
    while (foundobject and distance < maxsearchdist and destinationdist == -1 and redsignaldist == -1 and vehicledist == -1) {
      if (foundobject.isclass(Signal) and searcher.GetFacingRelativeToSearchDirection() and (cast<Signal>foundobject).GetSignalState() == Signal.RED) redsignaldist = distance; //���� ������ �������� ��������
      else if (foundobject.isclass(Vehicle)) {  //���� ������ ��������� �������
        vehicledist = distance; //���������� ��������� �� ��
        vehicledestlength = (cast<Vehicle>foundobject).GetLength(); //���������� ����� ��������� �������
      }
      Trackside trackside = (cast<Trackside>foundobject);
      if (trackside) {
        if (_target and _target.IsTargetObject(trackside, searcher.GetFacingRelativeToSearchDirection(), distance)) {
          currenttarget = trackside;
          destinationdist = distance;        
        } 
        float newspeadlimit = trackside.GetSpeedLimit();  
        if(searcher.GetFacingRelativeToSearchDirection() and !foundobject.isclass(Vehicle) and nextspeedboard == -1 and newspeadlimit > 0.001 and newspeadlimit < _maxspeed){
					nextspeedboard = newspeadlimit;
					nextspeedboarddist = distance;
				}
      }
      foundobject = searcher.SearchNext();
			distance = searcher.GetDistance();
    }

    float speadboardspeed = _maxspeed;    //��� ����� �������� �������� � ���������� ����������� ��������
    float destinationspeed = _maxspeed;   //��� ����� �������� �������� � ����
    float signalspeed = _maxspeed;        //��� ����� �������� �������� � ��������� ���������
    float vehiclespeed = _maxspeed;       //��� ����� �������� �������� � ��������� �������


    //���� ���� ����������� �������� � �� ���� ����� �����������, ��
 		if(!_maxspeedisconstant and nextspeedboard > 0)
			speadboardspeed = GetNewControlSpeed(nextspeedboarddist, nextspeedboard, 10, vehlenght * 0.5);

    //���� ������� ����, �� ����������� �������� �������� � ���
    if (destinationdist >= 0) {
      float offset = 0.5;
      if (currenttarget.isclass(Signal)) offset = 8.0;
      if (_target) offset = offset + _target.TargetStopOffset(currenttarget);
      destinationspeed = GetNewControlSpeed(destinationdist, 0.0, offset, vehlenght * 0.5);
    }

    //���� ��������� ����� ���� ��� �������� ��������, �� ����������� ��������� �������� �������� � ����
		if(!foundobject or redsignaldist >= 0){
      if (redsignaldist >= 0) signalspeed = GetNewControlSpeed(redsignaldist, 0.0, 8.0, vehlenght * 0.5);
      else if(!foundobject) signalspeed = GetNewControlSpeed(distance, 0.0, 3.0, vehlenght * 0.5); 
    }

    //���� ������� ��������� �������, �� ����������� �������� �������� � ���
    if(vehicledist >= 0) vehiclespeed = GetNewControlSpeed(vehicledist, 0.0, 15.0, (vehlenght + vehicledestlength) * 2);

    float newspeed = Math.Fmin(Math.Fmin(speadboardspeed, destinationspeed), Math.Fmin(signalspeed, vehiclespeed)); //���������� ����������� ��������� �������� ��������
    
    float speed = Math.Fabs(_train.GetTrainVelocity());
		if(speed >= 4.2 and speed > newspeed * _properties.CriticalSpeedFactor) StopTrain();
		else UpdatTraction(newspeed);

    if ((destinationdist > 0.0 and destinationdist < _properties.FullStopDist) and speed < 0.2) return RESULT_SUCCESS;
    else if (_target and _target.IsComplete()) return RESULT_TARGETCOMPLETE;
    return 0;
	}

  //������������ �������� �� ������
  final int DriveFor(Vehicle vehicle, GSTrackSearch searcher, float lastVehicleLength)
  {
    //if(!me.ignoreSignal and me.CheckStopAfterDrivingOfRedSignals()) return false; //���� ���� �������� �������, � ����� ��������� �������, �� �������� �������.

		//���� ���������� ��������� ����������� ��������, �� �������� ������� ����������
    if (!_maxspeedisconstant) {
      float advisorylimit = _train.GetAdvisoryLimit();
      if (/*me.supportAdvisoryLimit  and */advisorylimit > 0)
			  _maxspeed = Math.Fmin(advisorylimit,  _train.GetSpeedLimit());
      else _maxspeed = _train.GetSpeedLimit();
    }

    Trackside currenttarget;
		MapObject foundobject = searcher.SearchNext();
    float distance = searcher.GetDistance();
		float vehlenght = vehicle.GetLength();

		float nextspeedboard = -1; //��������� ����������� �������� (���� ���� ��������)
		float nextspeedboarddist = -1; //��������� �� ���������� ����������� ��������
    float destinationdist = -1; //��������� �� ����
    float redsignaldist = -1; //��������� �� �������� ������
    float vehicledist = -1; //��������� �� ������ ���������� �������
    float vehicledestlength = -1; //����� ��������� �������, � ������� ����������� ��������
		
    
		int maxsearchdist = 5000; // me.GetMaxObjectSearchDist(prop, 100);
    while (foundobject and distance < maxsearchdist and destinationdist == -1 and redsignaldist == -1 and vehicledist == -1) {
      if (foundobject.isclass(Signal) and searcher.GetFacingRelativeToSearchDirection() and (cast<Signal>foundobject).GetSignalState() == Signal.RED) redsignaldist = distance; //���� ������ �������� ��������
      else if (foundobject.isclass(Vehicle)) {  //���� ������ ��������� �������
        vehicledist = distance; //���������� ��������� �� ��
        vehicledestlength = (cast<Vehicle>foundobject).GetLength(); //���������� ����� ��������� �������
      }
      Trackside trackside = (cast<Trackside>foundobject);
      if (trackside) {
        if (_target and _target.IsTargetObject(trackside, searcher.GetFacingRelativeToSearchDirection(), distance)) {
          currenttarget = trackside;
          destinationdist = distance;        
        } 
        float newspeadlimit = trackside.GetSpeedLimit();  
        if(searcher.GetFacingRelativeToSearchDirection() and !foundobject.isclass(Vehicle) and nextspeedboard == -1 and newspeadlimit > 0.001 and newspeadlimit < _maxspeed){
					nextspeedboard = newspeadlimit;
					nextspeedboarddist = distance;
				}
      }
      foundobject = searcher.SearchNext();
			distance = searcher.GetDistance();
    }

    float speadboardspeed = _maxspeed;    //��� ����� �������� �������� � ���������� ����������� ��������
    float destinationspeed = _maxspeed;   //��� ����� �������� �������� � ����
    float signalspeed = _maxspeed;        //��� ����� �������� �������� � ��������� ���������
    float vehiclespeed = _maxspeed;       //��� ����� �������� �������� � ��������� �������


    //���� ���� ����������� �������� � �� ���� ����� �����������, ��
 		if(!_maxspeedisconstant and nextspeedboard > 0)
			speadboardspeed = GetNewControlSpeed(nextspeedboarddist, nextspeedboard, 10, vehlenght * 0.5);

    //���� ������� ����, �� ����������� �������� �������� � ���
    float backdist = -1.0;
    if (destinationdist >= 0 or _state & STATE_TARGETLOCK) {
      float frontdist = GetTargetDistanceFromLastVehicle(false, maxsearchdist); //���� ��������� �� ���������� ������
      if (frontdist >= 0 and !(_state & STATE_TARGETLOCK)) _state = ECLLogic.SetBitMask(_state, STATE_TARGETLOCK, true);
      else if (frontdist < 0 and _state & STATE_TARGETLOCK) _state = ECLLogic.SetBitMask(_state, STATE_TARGETFOLLOWED, true);
      if (_state & STATE_TARGETLOCK) {
        if (_state & STATE_TARGETFOLLOWED) {
          backdist = GetTargetDistanceFromLastVehicle(true, 3.0 * 2); //me.distanceMoveForObject * 2
          destinationdist = (lastVehicleLength * 0.5 + /*me.distanceMoveForObject*/ 3.0) - backdist;
          if(destinationdist < 0) destinationdist = 0;
        } else if(frontdist >= 0) destinationdist = (frontdist + lastVehicleLength) * 0.5 + 3.0; //me.distanceMoveForObject;
        destinationspeed = GetNewControlSpeed(destinationdist, 0.0, 0.0, 0.0);
      }
    }
    
    /*if (destinationdist >= 0) {     
      float offset = 0.5;
      if (currenttarget.isclass(Signal)) offset = 8.0;
      if (_target) offset = offset + _target.TargetStopOffset(currenttarget);
      destinationspeed = GetNewControlSpeed(destinationdist, 0.0, offset, vehlenght * 0.5);
    }*/

    //���� ��������� ����� ���� ��� �������� ��������, �� ����������� ��������� �������� �������� � ����
		if(!foundobject or redsignaldist >= 0){
      if (redsignaldist >= 0) signalspeed = GetNewControlSpeed(redsignaldist, 0.0, 8.0, vehlenght * 0.5);
      else if(!foundobject) signalspeed = GetNewControlSpeed(distance, 0.0, 3.0, vehlenght * 0.5); 
    }

    //���� ������� ��������� �������, �� ����������� �������� �������� � ���
    if(vehicledist >= 0) vehiclespeed = GetNewControlSpeed(vehicledist, 0.0, 15.0, (vehlenght + vehicledestlength) * 2);

    float newspeed = Math.Fmin(Math.Fmin(speadboardspeed, destinationspeed), Math.Fmin(signalspeed, vehiclespeed)); //���������� ����������� ��������� �������� ��������
    
    float speed = Math.Fabs(_train.GetTrainVelocity());
		if(speed >= 4.2 and speed > newspeed * _properties.CriticalSpeedFactor) StopTrain();
		else UpdatTraction(newspeed);

    if(_state & STATE_TARGETFOLLOWED and backdist > lastVehicleLength * 0.5 and speed < 0.2) {
     _state = ECLLogic.SetBitMask(_state, STATE_TARGETLOCK | STATE_TARGETFOLLOWED, false);
			return RESULT_SUCCESS;
		}
    if (_target and _target.IsComplete()) return RESULT_TARGETCOMPLETE;
    return 0;
	}




  //������������ �������� ����� ������
  final int DriveVia(Vehicle vehicle, GSTrackSearch searcher)
  {
    //if(!me.ignoreSignal and me.CheckStopAfterDrivingOfRedSignals()) return false; //���� ���� �������� �������, � ����� ��������� �������, �� �������� �������.

		//���� ���������� ��������� ����������� ��������, �� �������� ������� ����������
    if (!_maxspeedisconstant) {
      float advisorylimit = _train.GetAdvisoryLimit();
      if (/*me.supportAdvisoryLimit  and */advisorylimit > 0)
			  _maxspeed = Math.Fmin(advisorylimit,  _train.GetSpeedLimit());
      else _maxspeed = _train.GetSpeedLimit();
    }

		MapObject foundobject = searcher.SearchNext();
    float distance = searcher.GetDistance();
		float vehlenght = vehicle.GetLength();

		float nextspeedboard = -1; //��������� ����������� �������� (���� ���� ��������)
		float nextspeedboarddist = -1; //��������� �� ���������� ����������� ��������
    float destinationdist = -1; //��������� �� ����
    float redsignaldist = -1; //��������� �� �������� ������
    float vehicledist = -1; //��������� �� ������ ���������� �������
    float vehicledestlength = -1; //����� ��������� �������, � ������� ����������� ��������
		
    
		int maxsearchdist = 5000; // me.GetMaxObjectSearchDist(prop, 100);
    while (foundobject and distance < maxsearchdist and destinationdist == -1 and redsignaldist == -1 and vehicledist == -1) {
      if (foundobject.isclass(Signal) and searcher.GetFacingRelativeToSearchDirection() and (cast<Signal>foundobject).GetSignalState() == Signal.RED) redsignaldist = distance; //���� ������ �������� ��������
      else if (foundobject.isclass(Vehicle)) {  //���� ������ ��������� �������
        vehicledist = distance; //���������� ��������� �� ��
        vehicledestlength = (cast<Vehicle>foundobject).GetLength(); //���������� ����� ��������� �������
      }
      Trackside trackside = (cast<Trackside>foundobject);
      if (trackside) {
        if (_target and _target.IsTargetObject(trackside, searcher.GetFacingRelativeToSearchDirection(), distance)) destinationdist = distance;        
        float newspeadlimit = trackside.GetSpeedLimit();  
        if(searcher.GetFacingRelativeToSearchDirection() and !foundobject.isclass(Vehicle) and nextspeedboard == -1 and newspeadlimit > 0.001 and newspeadlimit < _maxspeed){
					nextspeedboard = newspeadlimit;
					nextspeedboarddist = distance;
				}
      }
      foundobject = searcher.SearchNext();
			distance = searcher.GetDistance();
    }

    float speadboardspeed = _maxspeed;    //��� ����� �������� �������� � ���������� ����������� ��������
    float signalspeed = _maxspeed;        //��� ����� �������� �������� � ��������� ���������
    float vehiclespeed = _maxspeed;       //��� ����� �������� �������� � ��������� �������


 		if(!_maxspeedisconstant and nextspeedboard > 0)
			speadboardspeed = GetNewControlSpeed(nextspeedboarddist, nextspeedboard, 10, vehlenght * 0.5);

    //���� ��������� ����� ���� ��� �������� ��������, �� ����������� ��������� �������� �������� � ����
		if(!foundobject or redsignaldist >= 0){
      if (redsignaldist >= 0) signalspeed = GetNewControlSpeed(redsignaldist, 0.0, 8.0, vehlenght * 0.5);
      else if(!foundobject) signalspeed = GetNewControlSpeed(distance, 0.0, 3.0, vehlenght * 0.5); 
    }

    //���� ������� ��������� �������, �� ����������� �������� �������� � ���
    if(vehicledist >= 0) vehiclespeed = GetNewControlSpeed(vehicledist, 0.0, 15.0, (vehlenght + vehicledestlength) * 2);

    float newspeed = Math.Fmin(speadboardspeed, Math.Fmin(signalspeed, vehiclespeed)); //���������� ����������� ��������� �������� ��������
    
    float speed = Math.Fabs(_train.GetTrainVelocity());
		if (speed >= 4.2 and speed > newspeed * _properties.CriticalSpeedFactor) StopTrain();
		else UpdatTraction(newspeed);

    if (!(_state & STATE_TARGETLOCK) and destinationdist > 0.0 and destinationdist < 25.0) _state = ECLLogic.SetBitMask(_state, STATE_TARGETLOCK, true);
    else if (_state & STATE_TARGETLOCK and destinationdist < 0.0) return RESULT_SUCCESS; 
    if (_target and _target.IsComplete()) return RESULT_TARGETCOMPLETE;
    return 0;
	}





























  public final int Start(int targetMove)
  {
    if (!_train) { Exception("AIDriver.Start> �� ���� ������� ������� ������������� Init(Train)."); return -1; }
    if (targetMove < 0 or targetMove > 2) { Exception("AIDriver.Start> �������� targetMove ����� ������������ ��������."); return -1; }
    if (!(_state & STATE_WORKED)) {
      _state = ECLLogic.SetBitMask(_state, STATE_WORKED, true);
      _state = ECLLogic.SetBitMask(_state, STATE_BREAK | STATE_TARGETLOCK | STATE_TARGETFOLLOWED, false);
      while (true) {
        if (_state & STATE_BREAK) return RESULT_BREAK;
        Vehicle[] vehicles = _train.GetVehicles();
        Vehicle vehicle = vehicles[0];
        GSTrackSearch searcher = vehicle.BeginTrackSearch(vehicle.GetDirectionRelativeToTrain());
        int result;
		    if(targetMove == TARGETMOVE_TO) result = DriveTo(vehicle, searcher);
		    else if(targetMove == TARGETMOVE_FOR) result = DriveFor(vehicle, searcher, vehicles[vehicles.size() - 1].GetLength());
		    else if(targetMove == TARGETMOVE_VIA) result = DriveVia(vehicle, searcher);
        if (result != 0) return result;
        Sleep(0.2);
      }
    }
    _state = ECLLogic.SetBitMask(_state, STATE_WORKED, false);
    return RESULT_ALREADYSTARTED;
  }

  public final int Start(int targetMove, AIDriverTargetBase target)
  {
    if (!_train) { Exception("AIDriver.Start> �� ���� ������� ������� ������������� Init(Train)."); return -1; }
    if (targetMove < 0 or targetMove > 2) { Exception("AIDriver.Start> �������� targetMove ����� ������������ ��������."); return -1; }
    if (_state & STATE_WORKED) return RESULT_ALREADYSTARTED;
    _target = target; 
    return Start(targetMove);
  }
  
  public final void Stop()
  {
    if (!_train) { Exception("AIDriver.Stop> �� ���� ������� ������� ������������� Init(Train)."); return; }

    _state = ECLLogic.SetBitMask(_state, STATE_BREAK, true);
    StopTrain();    
  }


  final void AbortHandler(Message msg) 
  {
    Stop();
  }

};