include "aicore.gs"

class AITrainProperties isclass GSObject
{
  public define int PRIORITY_STANDART         = 0;      //����������� ������� ���������
  public define int PRIORITY_MOTORCAR         = 1;      //��������� ��� �������������� ���������� �������
  public define int PRIORITY_CUSTOM           = 2;      //���������������� ���������

  public int priority                         = PRIORITY_STANDART;    //��� ����������� ����������
  public int priorityid                       = -1;                   //������������� ����������������� ����������     


};


class AIDriverBase isclass GameObject
{
  
  public mandatory AIDriverBase Init(Train train);
  
  public final AISpeedControlProperties GetProperties(void);
  
  public final void SetProperties(AISpeedControlProperties properties);
  
  public final void ResetProperties(void);
  
  public final float GetTrainMass(void);
  
  public final float GetTrainLength(void);
  
  public final void RecalcTrainMassAndLength(void);
  
  public float GetMaximumSpeed(void);
  
  public void SetMaximumSpeed(float maxSpeed);
  
  public void ResetMaximumSpeed(void);
  
  
                                                   
  
  
  final AICore GetCore(void);


  
  AICore _core;                                   //���� �������
  AISpeedControlProperties _properties;           //��������� �������� ��������
  Train _train;                                   //�������������� �����
  float _maxspeed;                                //����������� ���������� �������� ��������
  float _maxspeedthrottle;
  float _accfactor;                               //
  bool _maxspeedisconstant;                       //���������, �������� �� ����������� ���������� �������� ����������
  
  float _maxthrottle = -1.0;                      //������������ �������� ����, ��� ������� �������� �� ��������� ����������
  float _currthrottle = -1.0;                     //����, ��� ��������� ���������� 

  bool _resetbrakemode              = true;       //��������� ������������� ������ ������ ����������
  int _restbrakemodecounter;                      //������� �������� ��������� ������ ������ ����������

  float _trainmass;                               //����� �������
  float _trainlength;                             //����� �������  
  
  float _oldspeed;                                //�������� ������ ��� ��������� ����������
  float _oldcontrolspeed;                         //�������������� �������� ��� ��������� ����������



  public mandatory AIDriverBase Init(Train train)
  {
    if (_train) { Exception("AIDriverBase.Init> ������� ��� ���� ������� �����."); return null; }
    if (!train) { Exception("AIDriverBase.Init> �������� train �� ����� ����� �������� null."); return null; }
    _train = train;
    ResetProperties();
    RecalcTrainMassAndLength();
    ResetMaximumSpeed();
    return me;  
  }

  //���������� ��������� �������� ��������
  public final AISpeedControlProperties GetProperties(void)
  {
    if (!_train) { Exception("AIDriverBase.GetProperties> �� ���� ������� ������� ������������� Init(Train)."); return null; }
    return _properties;
  }

  //�������� ��������� �������� ��������
  //properties � ��������������� ��������� �������� ��������
  public final void SetProperties(AISpeedControlProperties properties)
  {
    if (!_train) { Exception("AIDriverBase.SetProperties> �� ���� ������� ������� ������������� Init(Train)."); return; }
    if (!properties) { Exception("AIDriverBase.SetProperties> �������� properties �� ����� ����� �������� null."); return; }
    _properties = properties; 
  }

  //���������� ��������� �������� �������� �� ��������������� ������� ������  
  public final void ResetProperties(void)
  {
    if (!_train) { Exception("AIDriverBase.ResetProperties> �� ���� ������� ������� ������������� Init(Train)."); return; } 
    AITrainProperties trainproperties = cast<AITrainProperties>_train.extension;
    AIPrioritySettingsBehavior prioritystorage = GetCore().GetPrioritySettingsBehavior();
    if (trainproperties) {
      if (trainproperties.priority == AITrainProperties.PRIORITY_CUSTOM and prioritystorage) {
        AICustomPriorityItem item = prioritystorage.FindCustomPriority(trainproperties.priorityid);
        if (item) {
          _train.SetTrainPriorityNumber(item.GetPriority());
          _properties = item.GetProperties();
          return;
        } 
      } else if (trainproperties.priority == AITrainProperties.PRIORITY_MOTORCAR) {
        _train.SetTrainPriorityNumber(1);
        if (prioritystorage) _properties = prioritystorage.GetSpeedControlProperties(0);
        else _properties = AIStandartSpeedControlProperties.CreateMotorcCar();
        return;  
      }
    }
    int priority = _train.GetTrainPriorityNumber();
    if (!prioritystorage) {
      if (priority == 3) _properties = AIStandartSpeedControlProperties.CreateShunt();
      else if (priority == 1) _properties = AIStandartSpeedControlProperties.CreatePassenger();
      else _properties = AIStandartSpeedControlProperties.CreateCargo();
    } else _properties = prioritystorage.GetSpeedControlProperties(priority);
  }

  //���������� ����� ������
  public final float GetTrainMass(void)
  {
    if (!_train) { Exception("AIDriverBase.GetTrainMass> �� ���� ������� ������� ������������� Init(Train)."); return 0.0; }
    return _trainmass;
  }
  
  //���������� ����� �������
  public final float GetTrainLength(void)
  {
    if (!_train) { Exception("AIDriverBase.GetTrainLength> �� ���� ������� ������� ������������� Init(Train)."); return 0.0; } 
    return _trainlength;
  }

  //��������������� ����� � ����� �������  
  public final void RecalcTrainMassAndLength(void)
  {
    if (!_train) { Exception("AIDriverBase.RecalcTrainMassAndLength> �� ���� ������� ������� ������������� Init(Train)."); return; }
    Vehicle[] vehicles = _train.GetVehicles();
    int i, count = vehicles.size();
    _trainmass = 0.0;
    _trainlength = 0.0;
    for (i = 0; i < count; ++i) {
      _trainmass = _trainmass + vehicles[i].GetMass();
      _trainlength = _trainlength + vehicles[i].GetLength(); 
    }
    _accfactor = _properties.StartAccFactor + (_properties.AccFactorStep * (_trainmass / _properties.AccFactorStepMass));
  }

  //���������� ����������� ���������� �������� ��������  
  public float GetMaximumSpeed(void)
  {
    if (!_train) { Exception("AIDriverBase.GetMaximumSpeed> �� ���� ������� ������� ������������� Init(Train)."); return 0.0; }
    return _maxspeed; 
  }

  //����� ����������� ���������� ��������  
  public void SetMaximumSpeed(float maxSpeed)
  {
    if (!_train) { Exception("AIDriverBase.SetMaximumSpeed> �� ���� ������� ������� ������������� Init(Train)."); return; }
    if (maxSpeed <= 0.0) { GetCore().Exception("AIDriverBase.SetMaximumSpeed> �������� ��������� maxSpeed ������ ���� ������ 0."); return; }
    _maxspeed = _maxspeed;
    _maxspeedisconstant = true; 
  }
  
  //���������� ������������ �������� �� ��������������� �������� �������  
  public void ResetMaximumSpeed(void)
  {
    if (!_train) { Exception("AIDriverBase.SetMaximumSpeed> �� ���� ������� ������� ������������� Init(Train)."); return; }
    _maxspeed = _train.GetSpeedLimit();
    _maxspeedisconstant = false; 
  }











  

  //���������� ������ �� ����
  final AICore GetCore(void) 
  {
    if (!_core) {
      Soup kuidconvertsoup = Constructors.NewSoup();
      kuidconvertsoup.SetNamedTag("core-kuid", "<kuid:151055:80000>");
      _core = cast<AICore>TrainzScript.GetLibrary(kuidconvertsoup.GetNamedTagAsKUID("core-kuid"));
    }
    return _core;
  }





  final void UpdatTraction(float controlSpeed)
  {
    float accfactor = 1.0, speed = Math.Fabs(_train.GetTrainVelocity());
    if (speed) accfactor = _oldspeed / speed;                                               //���������� ��������� (<0 - ���������, >0 - ����������)
    if (_train.IsStopped()) _resetbrakemode = true;                                         //���� ����� �����, �� ������ ����� ������ ������ ����������
    if (controlSpeed > _oldcontrolspeed) _maxthrottle = 1.0;
    if (controlSpeed < _maxspeed and !_resetbrakemode) {
      float factor = controlSpeed / _maxspeed;                                              //��������� ������� �������������� �������� �� ����������� ����������
      float newthrottle = _maxspeedthrottle * factor;
      if (newthrottle < _properties.StartThrottle) newthrottle = _properties.StartThrottle; //������� ���� �� ����� ���� ���� �������
      if (_train.GetDCCThrottle() >= newthrottle and (controlSpeed == 0.0 or speed / controlSpeed > 0.8)) accfactor = 0.0;
      if (_oldspeed == speed and ++_restbrakemodecounter >= 20) _resetbrakemode = true;
      else if (_oldspeed != speed) _restbrakemodecounter = 0;
    }
    if(accfactor >= _accfactor and speed < controlSpeed and (_currthrottle < 0 or 
    _maxthrottle < 0 or _train.GetDCCThrottle() < _maxthrottle) and controlSpeed > 0){
      _currthrottle = _train.GetDCCThrottle();
      if (_currthrottle <= 0.0) {
        _train.SetDCCThrottle(_properties.StartThrottle);
        if (_properties.PauseAfterFirstThrottle > 0)
          Sleep(_properties.PauseAfterFirstThrottle);
      } else _train.SetDCCThrottle(_currthrottle + _properties.AccelStep);
    } else if (speed > controlSpeed) {
      float currthrottle = _train.GetDCCThrottle();
      _resetbrakemode = false;
      _restbrakemodecounter = false;
      if (currthrottle <= _currthrottle) _currthrottle = -1.0;
      else {
        _maxthrottle = _currthrottle;
        _maxspeedthrottle = _currthrottle;
      }
      if(currthrottle - _properties.DecelStep > 0) _train.SetDCCThrottle(currthrottle - _properties.DecelStep);
      else {
        _train.SetDCCThrottle(0.0);
        _currthrottle = -1.0;
      }      
    }
    _oldspeed = speed;
    _oldcontrolspeed = controlSpeed;  
  }


	final float GetNewControlSpeed(float distance, float requiredSpeed, float offset, float blindSpot)
  {
		float retspeed = _maxspeed, originaldistance = distance;
		distance = distance - blindSpot - offset; //���������� �������� ���������� �� ������� � ������ ������ ���� � �������� �����
    if (requiredSpeed > 0 or distance > _properties.ReducedSpeedDist) { //����  ������� �������� ������ 0 ��� ���������� ������ ��� ���������� ������ �������
      float requiredspeed = requiredSpeed, requiredspeeddist = 0.0; 
      if (requiredSpeed == 0) { //���� ��������� �������� 0 (����� ������������)
				requiredspeed = _properties.ReducedSpeed;  //���������� �������� �������
        requiredspeeddist = _properties.ReducedSpeedDist; //���������� ���������� �� ������ �������
      }
      float reducinglength = (_maxspeed - requiredspeed) * 3.6 * _properties.BrakeFactor; //���������� ����� ���������� ���� �� ��������� �������
      if (distance - requiredspeeddist < reducinglength) { //���� ���������� �� ��������� �������� ������ ��� ����� ���������� ����
        float cdistance = distance - requiredspeeddist; //���������� ���������� �� ����� ��������� ��������
        if (cdistance < 0) cdistance = 0;
        retspeed = requiredspeed + (cdistance / _properties.BrakeFactor  / 3.6); //���������� ��������� �������� � ������� �����
				if (retspeed <= 0.2) {
					_train.SetDCCThrottle(0);
					_maxthrottle = -1.0;
					retspeed = 0;
				} else if (retspeed > _maxspeed) retspeed = _maxspeed;
      }
    } else if(distance < _properties.FullStopDist) { //���� ��������� ������ ��� ��������� ���� ������ ���������
			retspeed = 0.2 + _properties.ReducedSpeed * (distance / _properties.FullStopDist); //���������� ��������� �������� � ������� �����
			if (retspeed <= 0.2 or (retspeed < 0.5 and originaldistance < blindSpot)) {
				_train.SetDCCThrottle(0);
        _maxthrottle = -1.0;
        retspeed = 0;
			}
    } else if (distance > _properties.FullStopDist and distance <= _properties.ReducedSpeedDist) retspeed = _properties.ReducedSpeed; //���� ��������� ������ ���������� ���� � � �������� �������, �� ������ �������� �������
		return retspeed;  
  }


  final void StopTrain()
  {
  }


};