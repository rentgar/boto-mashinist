include "aistopmarkerbase.gs"


class AIDriverTargetBase
{

  public bool IsTargetObject(Trackside trackObject, bool facingRelativeToSearch, float distance) { return false; }

  public float TargetStopOffset(Trackside trackObject) { return 0.0; }
  
  public bool IsComplete(void) { return false; }

};

class AIDriverTargetTrackMark isclass AIDriverTargetBase
{

  public final TrackMark GetTrackMark(void);

  public final void SetTrackMark(TrackMark marker);


  TrackMark _marker;
 

  public final TrackMark GetTrackMark(void)
  {
    return _marker;
  }

  public final void SetTrackMark(TrackMark marker)
  {
    if (!marker) { Interface.Exception("AIDriverTargetTrackMark.SetTrackMark> Значение аргумента marker не может быть null"); return; }
    _marker = marker;
  }

  public bool IsTargetObject(Trackside trackObject, bool facingRelativeToSearch, float distance) 
  { 
    return _marker and trackObject == _marker;  
  }

};

class AIDriverTargetStopMarker isclass AIDriverTargetBase
{

  public define int TYPE_ANYMARKER = -1;
  
  public final string StationName(void);
  
  public final void SetStation(string stationName);
  
  public final int MerkerType(void);
  
  public final void SetMarkerType(int type);
  
  public final int SectionCount(void);
  
  public final void SetSectionCount(int sectonCount); 
  

  string _stationname;                        //Наименвоанеи остановочного пункта, до маркера которого нужно ехать
  int _markertype         = TYPE_ANYMARKER;   //Тип маркера до которого нужно ехать
  int _sectioncount       = 0;                //Количество секций для некоторых типов маркеров  


  public final string StationName(void)
  {
    return _stationname;
  }
  
  public final void SetStation(string stationName)
  {
    if (stationName) {
      Str.TrimLeft(stationName, " ");
      Str.TrimRight(stationName, " ");
      if (!stationName.size()) stationName = null;
    }
    _stationname = stationName; 
  }
  
  public final int MerkerType(void)
  {
    return _markertype;
  }
  
  public final void SetMarkerType(int type)
  {
    if (type < -1 or type > 3) { Interface.Exception("AIDriverTargetStopMarker.SetMarkerType> Недопустимое значение аргумента type"); return; }
    if (_markertype != type) {
      _markertype = type;
      _sectioncount = 0;
    }
  }
  
  public final int SectionCount(void)
  {
    if (_markertype == AIStopMarkerBase.TYPE_LOCOMOTIVE and _markertype == AIStopMarkerBase.TYPE_CONSIST) return _sectioncount;
    return -1;
  }
  
  public final void SetSectionCount(int sectonCount)
  {                                                 
    if (_markertype != AIStopMarkerBase.TYPE_LOCOMOTIVE and _markertype != AIStopMarkerBase.TYPE_CONSIST) { Interface.Exception("AIDriverTargetStopMarker.SetSectionCount> Для данного типа маркера невозможно установить количество секций."); return; }
    if (sectonCount < 0 or ((_markertype == AIStopMarkerBase.TYPE_LOCOMOTIVE and sectonCount <= 4) or sectonCount <= 8)) { Interface.Exception("AIDriverTargetStopMarker.SetMarkerType> Недопустимое значение аргумента sectonCount"); return; }
    _sectioncount = sectonCount;
  } 

  public bool IsTargetObject(Trackside trackObject, bool facingRelativeToSearch, float distance) 
  {
    if (!facingRelativeToSearch) return false;
    AIStopMarkerBase marker = cast<AIStopMarkerBase>trackObject;
    if ( marker) {
      if (_stationname and _stationname != marker.StationName()) return false;
      if (_markertype == TYPE_ANYMARKER) return true;
      if (_markertype != marker.Type()) return false;
      return (_markertype != AIStopMarkerBase.TYPE_CONSIST and _markertype != AIStopMarkerBase.TYPE_CONSIST) or _sectioncount == 0 or marker.SectionCount() == 0 or _sectioncount != marker.SectionCount();
    }
    return false;
  }


}; 