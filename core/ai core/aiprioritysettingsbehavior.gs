//=============================================================================
// File: aiprioritysettingsbehavior.gs
// Desc: Базовый скрипт для правила настроек приоритетов 
// Auth: Алексей 'Эрендир' Зверев 2020 © Trainz Dev Team
//       Licence: GNU GPL
//=============================================================================

include "scenariobehavior.gs"
include "aiproperties.gs"
include "aicore.gs"

//=============================================================================
// Name: AIPrioritySettingsBehavior
// Desc: Базовый класс для правила настройки приоритетов
//=============================================================================
class AIPrioritySettingsBehavior isclass ScenarioBehavior
{

  //=============================================================================
  // Name: GetSpeedControlProperties
  // Desc: Получает параметры контроля скорости для одного из стандартных 
  //       приоритетов.
  // Parm: priority – игровой приоритет для этой записи, где:
  //       0 — приоритет для МВПС
  //       1 — пассажирский приоритет;
  //       2 — грузовой приоритет;
  //       3 — маневровый приоритет
  // Retn: Объект AISpeedControlProperties, представляющий параметры контроля 
  //       скорости 
  //=============================================================================
  public final AISpeedControlProperties GetSpeedControlProperties(int priority);
  
  //=============================================================================
  // Name: FindCustomPriority
  // Desc: Выполняет поиск пользовательского приоритета.
  // Parm: itemId — уникальный идентификатор приоритета.
  // Retn: Объект AICustomPriorityItem, представляющий параметры 
  //       пользовательского приоритета. 
  //=============================================================================
  public final AICustomPriorityItem FindCustomPriority(int itemId);
  
  //=============================================================================
  // Name: CustomPriorityItem
  // Desc: Возвращает параметры пользовательского приоритета.
  // Parm: index — индекс записи параметров пользовательского приоритета в
  //       коллекции.
  // Retn: Объект AICustomPriorityItem, представляющий параметры 
  //       пользовательского приоритета. 
  //=============================================================================
  public final AICustomPriorityItem CustomPriorityItem(int index);

  //=============================================================================
  // Name: CustomPriorityItemCount
  // Desc: Возвращает количество записей о параметрах пользовательского
  //       интерфейса в коллекции.
  // Retn: Количество записей о параметрах пользовательского интерфейса в
  //       коллекции. 
  //=============================================================================
  public final int CustomPriorityItemCount(void);
  
  //=============================================================================
  // Name: IsActive
  // Desc: Возвращает значение, указывающее, является ли это правило активным.
  // Retn: Значение true, если правило является активным; в противном случае —
  //       значение false, если есть другое активнойе правило 
  //=============================================================================
  public final bool IsActive(void);
  
  //=============================================================================
  // Name: CreateCustomPriorityItem
  // Desc: Создаёт новую запись о параметрах пользовательского приоритета.
  // Parm: name — наименвоанеи пользовательского приоритета.
  // Parm: priority – игровой приоритет для этой записи, где:
  //       1 — пассажирский приоритет;
  //       2 — грузовой приоритет;
  //       3 — маневровый приоритет
  // Retn: Объект AICustomPriorityItem, представляющий созданные параметры 
  //       пользовательского приоритета. 
  //=============================================================================
  final AICustomPriorityItem CreateCustomPriorityItem(string name, int priority);
  
  //Меняяет местами две записи о парамерах пользовательского приоритета.
  // Parm: indexX — отсчитываемый от нуля индекс первой записи.
  // Parm: indexY — отсчитываемый от нуля индекс второй записи.
  final void SwapCustomPriorityItems(int indexX, int indexY);
  
  //=============================================================================
  // Name: RemoveCustomPriorityItemAt
  // Desc: Удаляет параметры пользовательского приоритета из колекции по его
  //       индексу.
  // Parm: index — индекс записи параметров пользовательского приоритета в
  //       коллекции.
  //=============================================================================
  final void RemoveCustomPriorityItemAt(int index);
  
  //=============================================================================
  // Name: RemoveCustomPriorityItem
  // Desc: Удаляет параметры пользовательского приоритета из колекции.
  // Parm: itemId — уникальный идентификатор приоритета.
  // Retn: Значенеи true, если запись удачно была удалена; значение false,
  //       если запись о пользовательском приоритете с указанным itemId не
  //       существует в коллекции.  
  //=============================================================================
  final bool RemoveCustomPriorityItem(int itemId);
  
  //=============================================================================
  // Name: CustomPriorityItemChangeNotice
  // Desc: Уведомляет ядро об изменении параметров пользовательского приоритета.
  //=============================================================================
  final void CustomPriorityItemChangeNotice(void);
  
  //=============================================================================
  // Name: GetCore
  // Desc: Возвращает ссылку на ядро.
  // Retn: Объект AICore, представляющий ядро системы.
  //=============================================================================
  final AICore GetCore(void); 
  
	//
	// РЕАЛИЗАЦИЯ
	//
  
  AICore _core;
  AISpeedControlProperties[] _properties;
  AICustomPriorityItem[] _items;

  public final AISpeedControlProperties GetSpeedControlProperties(int priority)
  {
    if(priority < 0 or priority > 3) { Exception("Недопустимое значение аргумента priority."); return null; }
    return _properties[priority]; 
  }
  
  public final AICustomPriorityItem FindCustomPriority(int itemId)
  {
    if(itemId < 0) return null;
    int i, count = _items.size();
    for(i = 0; i < count; ++i)
      if(_items[i].GetId() == itemId) return _items[i];
    return null;
  }

  public final AICustomPriorityItem CustomPriorityItem(int index)
  {
    if(index < 0 or index >= _items.size()) { Exception("Индекс выходит за пределы колекции."); return null; }
    return _items[index];
  }

  public final int CustomPriorityItemCount(void)
  {
    return _items.size();
  }
  
  public final bool IsActive(void)
  {
    AIPrioritySettingsBehavior activebehavior = _core.GetPrioritySettingsBehavior();
    return activebehavior == me or _core.SetPrioritySettingsBehavior(me, false); 
  }

  final AICustomPriorityItem CreateCustomPriorityItem(string name, int priority)
  {
    Str.TrimLeft(name, " ");
    Str.TrimRight(name, " ");
    if(name == "") { Exception("Не указано наименование приоритета."); return null; }
    if(priority < 1 or priority > 3) { Exception("Недопустимое значение аргумента priority."); return null; }
    AICustomPriorityItem newitem = new AICustomPriorityItem().Init(_core.NewId(), name, priority);
    _items[_items.size()] = newitem;
    CustomPriorityItemChangeNotice();
    return newitem;
  }
  
  final void SwapCustomPriorityItems(int indexX, int indexY)
  {
    if (indexX < 0 or indexX >= _items.size()) { 
      Exception("AIPrioritySettingsBehavior.SwapCustomPriorityItems> Аргумент indexX содержит индекс, который выходит за границы коллекции."); 
      return;
    }
    if (indexY < 0 or indexY >= _items.size()) { 
      Exception("AIPrioritySettingsBehavior.SwapCustomPriorityItems> Аргумент indexY содержит индекс, который выходит за границы коллекции."); 
      return;
    }
    AICustomPriorityItem moveitem = _items[indexX];
    _items[indexX] = _items[indexY];
    _items[indexY] = moveitem;
    CustomPriorityItemChangeNotice();
  }
  
  
  final void RemoveCustomPriorityItemAt(int index)
  {
    if(index < 0 or index >= _items.size()) { Exception("Индекс выходит за пределы колекции."); return; }
    _items[index, index + 1] = null;
    CustomPriorityItemChangeNotice();
  }

  final bool RemoveCustomPriorityItem(int itemId)
  {
    if(itemId < 0) return false;
    int i, count = _items.size();
    for(i = 0; i < count; ++i) {
      if(_items[i].GetId() == itemId){
        RemoveCustomPriorityItemAt(i);
        return true;
      } 
    }
    return false;
  }

  final void CustomPriorityItemChangeNotice(void)
  {
    PostMessage(_core, "AIDriver", "PriorityChanged", 0.0);
  }
  
  final Soup GetSpeedControlPropertiesSoup(AISpeedControlProperties properties)
  {
    Soup soup = Constructors.NewSoup();
    soup.SetNamedTag("StartThrottle", properties.StartThrottle);
    soup.SetNamedTag("AccFactor", properties.AccFactor);
    soup.SetNamedTag("StartAccFactor", properties.StartAccFactor);
    soup.SetNamedTag("AccFactorStep", properties.AccFactorStep);
    soup.SetNamedTag("AccFactorStepMass", properties.AccFactorStepMass);
    soup.SetNamedTag("AccelStep", properties.AccelStep);
    soup.SetNamedTag("DecelStep", properties.DecelStep);
    soup.SetNamedTag("BrakeFactor", properties.BrakeFactor);
    soup.SetNamedTag("FullStopDist", properties.FullStopDist);
    soup.SetNamedTag("ReducedSpeedDist", properties.ReducedSpeedDist);
    soup.SetNamedTag("ReducedSpeed", properties.ReducedSpeed);
    soup.SetNamedTag("CriticalSpeedFactor", properties.CriticalSpeedFactor);
    soup.SetNamedTag("PauseAfterFirstThrottle", properties.PauseAfterFirstThrottle);
    return soup;
  }
  
  final Soup GetCustomPrioritySoup(AICustomPriorityItem item)
  {
    Soup soup = Constructors.NewSoup();
    soup.SetNamedTag("Id", item.GetId());
    soup.SetNamedTag("Name", item.GetName());
    soup.SetNamedTag("Priority", item.GetPriority());
    soup.SetNamedSoup("Properties", GetSpeedControlPropertiesSoup(item.GetProperties()));
    return soup;
  }

  final AISpeedControlProperties RestoreSpeedControlProperties(Soup soup, AISpeedControlProperties defaultProperties)
  {
    AISpeedControlProperties properties = new AISpeedControlProperties();
    properties.StartThrottle              = soup.GetNamedTagAsFloat("StartThrottle", defaultProperties.StartThrottle);
    properties.AccFactor                  = soup.GetNamedTagAsFloat("AccFactor", defaultProperties.AccFactor);
    properties.StartAccFactor             = soup.GetNamedTagAsFloat("StartAccFactor", defaultProperties.StartAccFactor);
    properties.AccFactorStep              = soup.GetNamedTagAsFloat("AccFactorStep", defaultProperties.AccFactorStep);
    properties.AccFactorStepMass          = soup.GetNamedTagAsFloat("AccFactorStepMass", defaultProperties.AccFactorStepMass);
    properties.AccelStep                  = soup.GetNamedTagAsFloat("AccelStep", defaultProperties.AccelStep);
    properties.DecelStep                  = soup.GetNamedTagAsFloat("DecelStep", defaultProperties.DecelStep);
    properties.BrakeFactor                = soup.GetNamedTagAsFloat("BrakeFactor", defaultProperties.BrakeFactor);
    properties.FullStopDist               = soup.GetNamedTagAsInt("FullStopDist", defaultProperties.FullStopDist);
    properties.ReducedSpeedDist           = soup.GetNamedTagAsInt("ReducedSpeedDist", defaultProperties.ReducedSpeedDist);
    properties.ReducedSpeed               = soup.GetNamedTagAsFloat("ReducedSpeed", defaultProperties.ReducedSpeed);
    properties.CriticalSpeedFactor        = soup.GetNamedTagAsFloat("CriticalSpeedFactor", defaultProperties.CriticalSpeedFactor);
    properties.PauseAfterFirstThrottle    = soup.GetNamedTagAsFloat("PauseAfterFirstThrottle", defaultProperties.PauseAfterFirstThrottle);
    return properties;
  }
  
  final AICustomPriorityItem RestoreCustomPriorityItem(Soup soup)
  {
    int id = soup.GetNamedTagAsInt("Id", -1); 
    int priority = soup.GetNamedTagAsInt("Priority", -1);
    string name = soup.GetNamedTag("Name");
    if(id >= 0 and priority >= 1 and priority <= 3 and name != "") {
      AICustomPriorityItem item = new AICustomPriorityItem().Init(id, name, priority);
      item.SetProperties(RestoreSpeedControlProperties(soup.GetNamedSoup("Properties"), item.GetProperties()));
      return item;
    }
    return null;  
  }
  
  public mandatory Soup GetProperties(void)
  {
    Soup soup = inherited();
    Soup propertiessoup = Constructors.NewSoup();
    Soup prioritiessoup = Constructors.NewSoup();
    int i, count = _properties.size();
    for(i = 0; i < count; ++i)
      propertiessoup.SetNamedSoup(i, GetSpeedControlPropertiesSoup(_properties[i]));
    count = _items.size();
    for(i = 0; i < count; ++i)
      prioritiessoup.SetNamedSoup(i, GetCustomPrioritySoup(_items[i]));
    soup.SetNamedSoup("Properties", propertiessoup);
    soup.SetNamedSoup("Priorities", prioritiessoup);
    soup.SetNamedTag("IsActive", IsActive());
    return soup;
  }

  public mandatory void SetProperties(Soup soup)
  {
    inherited(soup);
    Soup propertiessoup = soup.GetNamedSoup("Properties");
    Soup prioritiessoup = soup.GetNamedSoup("Priorities");
    int i, count = _properties.size();
    for(i = 0; i < count; ++i)
      _properties[i] = RestoreSpeedControlProperties(propertiessoup.GetNamedSoup(i), _properties[i]);
    count = prioritiessoup.CountTags();
    _items = new AICustomPriorityItem[0];
    for(i = 0; i < count; ++i) {
      AICustomPriorityItem item = RestoreCustomPriorityItem(prioritiessoup.GetNamedSoup(prioritiessoup.GetIndexedTagName(i)));
      if(item) _items[_items.size()] = item;
    }
    if(soup.GetNamedTagAsBool("IsActive")) _core.SetPrioritySettingsBehavior(me, true); 
    else CustomPriorityItemChangeNotice();
  }
  
  public mandatory void Init(Asset asset)
  {
    inherited(asset);
    _core = cast<AICore>TrainzScript.GetLibrary(asset.LookupKUIDTable("ai-driver-core"));
    _properties = new AISpeedControlProperties[4];
    _properties[0] = AIStandartSpeedControlProperties.CreateMotorcCar();
    _properties[1] = AIStandartSpeedControlProperties.CreatePassenger();
    _properties[2] = AIStandartSpeedControlProperties.CreateCargo();
    _properties[3] = AIStandartSpeedControlProperties.CreateShunt();
    _items = new AICustomPriorityItem[0];
    _core.SetPrioritySettingsBehavior(me, false);
  }
  
  public final string GetChildRelationshipIcon(ScenarioBehavior child) 
  { 
    return "none"; 
  }
  
  final AICore GetCore(void)
  {
    return _core;
  }
   


};