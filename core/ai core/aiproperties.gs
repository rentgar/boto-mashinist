//=============================================================================
// File: aiproperties.gs
// Desc: Содержит описание классов для настройки бото-машиниста.
// Auth: Алексей 'Эрендир' Зверев 2020 © Trainz Dev Team
//       Licence: GNU GPL
//=============================================================================

include "interface.gs"

//=============================================================================
// Name: AISpeedControlProperties
// Desc: Описывает параметры для контроля скорости бото-машинистом.
// Note: Заданые значения по умолчанию соответствуют грузовому приоритету
//=============================================================================
final class AISpeedControlProperties isclass GSObject
{

  //=============================================================================
  // Name: StartThrottle
  // Desc: Стартове значение тяги, которое будет задано при начале движения.
  //=============================================================================
	public float StartThrottle            = 0.01; 

  //=============================================================================
  // Name: AccFactor
  // Desc: Коэфициент ускарения. Влияет на скорость набора тяги (0...1). 
  //=============================================================================
	public float AccFactor                = 0.999; 

  //=============================================================================
  // Name: StartAccFactor
  // Desc: Коэфициент ускарения. Влияет на скорость набора тяги (0...1). 
  ///      Используется при расчёте в зависимости от массы состава.
  //=============================================================================
	public float StartAccFactor           = 0.95; 

  //=============================================================================
  // Name: AccFactorStep
  // Desc: Шаг изменения коэфициента тяги в зависимости от массы состава на 
  ///      единицу массы.
  //=============================================================================
	public float AccFactorStep            = 0.004083;

  //=============================================================================
  // Name: AccFactorStepMass
  // Desc: Единица массы (кг) для расчёта шага кэфициента тяги. 
  //=============================================================================
	public float AccFactorStepMass        = 500000; 
                                                
  //=============================================================================
  // Name: AccelStep
  // Desc: Шаг наборя тяги. 
  //=============================================================================
	public float AccelStep                = 0.01; 

  //=============================================================================
  // Name: DecelStep
  // Desc: Шаг сброса тяги.
  //=============================================================================
	public float DecelStep                = 0.01; 

  //=============================================================================
  // Name: BrakeFactor
  // Desc: Коэфициент расчёта начала торможения.
  //=============================================================================
	public float BrakeFactor              = 14; 

  //=============================================================================
  // Name: FullStopDist
  // Desc: Растояние до цели, на котором нужно начинать полную остановку.
  //=============================================================================
	public int FullStopDist               = 50; 

  //=============================================================================
  // Name: ReducedSpeedDist
  // Desc: Растояние до цели, на котором нужно начинать двигаться с ументшеной
  //       скоростью
  //=============================================================================
	public int ReducedSpeedDist           = 150; 

  //=============================================================================
  // Name: ReducedSpeed
  // Desc: Скорость (м/с), с которой нужно двигаться начиная с растояни, 
  //       указаного в ReducedSpeedDist.
  //=============================================================================
	public float ReducedSpeed             = 2.8; 

  //=============================================================================
  // Name: CriticalSpeedFactor
  // Desc: Коэфициент (> 1), дря расчёта превышения скорости, после которого
  //       сработает экстреное торможение.
  //=============================================================================
	public float CriticalSpeedFactor      = 1.7; //Коэффициент критической скорости для расчёта экстреного торможения
                                                
  //=============================================================================
  // Name: PauseAfterFirstThrottle
  // Desc: Пауза при начала движения после установки тяги StartThrottle до
  //       начала её дальнейшего набора.
  //=============================================================================
	public float PauseAfterFirstThrottle  = 0; //Пауза после установки первой позиции тяги

};

//=============================================================================
// Name: AIStandartSpeedControlProperties
// Desc: Предоставляет генератор стандартных параметров контроля скорости.
//=============================================================================
static class AIStandartSpeedControlProperties
{

  //=============================================================================
  // Name: CreatePassenger
  // Desc: Создаёт параметры контроля скорости соответствующие пассажирскому 
  //       поезду.
  // Retn: Объект AISpeedControlProperties, содержащий параметры контроля 
  //       скорости.
  //=============================================================================
  public AISpeedControlProperties CreatePassenger()
  {
    AISpeedControlProperties param = new AISpeedControlProperties();
		param.StartThrottle = 0.03;
		param.AccFactor = 0.9995;
	  param.AccFactorStep = 0.0099;
	  param.AccFactorStepMass = 200000;
		param.FullStopDist = 65;
		param.ReducedSpeedDist = 150;
		param.ReducedSpeed = 4.2;
		param.PauseAfterFirstThrottle = 2;
    return param;
  }  
  
  //=============================================================================
  // Name: CreatePassenger
  // Desc: Создаёт параметры контроля скорости соответствующие грузовому поезду.
  // Retn: Объект AISpeedControlProperties, содержащий параметры контроля 
  //       скорости.
  //=============================================================================
  public AISpeedControlProperties CreateCargo()
  {
    return new AISpeedControlProperties();
  }
  
  //=============================================================================
  // Name: CreatePassenger
  // Desc: Создаёт параметры контроля скорости соответствующие маневровому 
  //       составу.
  // Retn: Объект AISpeedControlProperties, содержащий параметры контроля 
  //       скорости.
  //=============================================================================
  public AISpeedControlProperties CreateShunt()
  {
    AISpeedControlProperties param = new AISpeedControlProperties();
		param.FullStopDist = 25;
		param.ReducedSpeedDist = 40;
		param.ReducedSpeed = 3.3;
	  param.AccFactorStep = 0.0099;
	  param.AccFactorStepMass = 200000;
	  param.BrakeFactor = 6;
    return param;
  }
  
  public AISpeedControlProperties CreateMotorcCar()
  {
    AISpeedControlProperties param = new AISpeedControlProperties();
		param.StartThrottle = 0.08;
		param.AccFactor = 0.95;
		param.StartAccFactor = 0.90;
	  param.AccFactorStep = 0.004083;
	  param.AccFactorStepMass = 700000;
		param.FullStopDist = 200;
		param.ReducedSpeedDist = 300;
		param.ReducedSpeed = 18;
		param.PauseAfterFirstThrottle = 1;
    return param;
  }
  

}; 


//=============================================================================
// Name: AICustomPriorityItem
// Desc: Предоставляет элемент, описывающий пользовательский приоритет. 
//=============================================================================
final class AICustomPriorityItem isclass GSObject
{

  //=============================================================================
  // Name: Init
  // Desc: Инициализация нового экземпляра записи.
  // Parm: id – уникальный идентификатор записи.
  // Parm: name – наименованеи записи.
  // Parm: priority – игровой приоритет для этой записи, где:
  //       1 — пассажирский приоритет;
  //       2 — грузовой приоритет;
  //       3 — маневровый приоритет
  // Retn: Объект AICustomPriorityItem, представляющий инициализированную запись.
  //=============================================================================
  public AICustomPriorityItem Init(int id, string name, int priority);

  //=============================================================================
  // Name: GetId
  // Desc: Возвращает уникальный идентификатор записи.
  // Retn: Уникальный идентификатор записи.
  //=============================================================================
  public int GetId(void);
  
  //=============================================================================
  // Name: GetName
  // Desc: Возвращает наименвоанеи записи.
  // Retn: Наименвоание записи.
  //=============================================================================
  public string GetName(void);
  
  //=============================================================================
  // Name: SetName
  // Desc: Задаёт наименование записи
  // Parm: name – новое наименование записи.
  //=============================================================================
  public void SetName(string name);
  
  //=============================================================================
  // Name: GetPriority
  // Desc: Возвращает игровой приоритет
  // Retn: Игровой приоритет для этой записи, где:
  //       1 — пассажирский приоритет;
  //       2 — грузовой приоритет;
  //       3 — маневровый приоритет
  //=============================================================================
  public int GetPriority(void);
  
  //=============================================================================
  // Name: SetPriority
  // Desc: Назначает игровой идентификатор записи.
  // Parm: priority – игровой приоритет для этой записи, где:
  //       1 — пассажирский приоритет;
  //       2 — грузовой приоритет;
  //       3 — маневровый приоритет
  // Parm: resetProperties – значение true, если требуется сбросить параметры
  //       контроля скорости на значения по умолчанию для указаного приоритета;
  //       в противном случае — значенеи false.
  //=============================================================================
  public void SetPriority(int priority, bool resetProperties);

  //=============================================================================
  // Name: SetPriority
  // Desc: Назначает игровой идентификатор записи без сброса параметров контроля
  //       скорости.
  // Parm: priority – игровой приоритет для этой записи, где:
  //       1 — пассажирский приоритет;
  //       2 — грузовой приоритет;
  //       3 — маневровый приоритет
  //=============================================================================
  public void SetPriority(int priority) { SetPriority(priority, false); }
  
  //=============================================================================
  // Name: GetProperties
  // Desc: Возвращает параметры контроля скорости для этой записи.
  // Retn: Объект AISpeedControlProperties, представляющий параметры контроля
  //       скорости.
  //=============================================================================
  public AISpeedControlProperties GetProperties(void);
  
  //=============================================================================
  // Name: SetProperties
  // Desc: Устанавливает параметры контроля скорости для этой записи.
  // Parm: properties – объект AISpeedControlProperties, который представлят
  //       параметры контроля скорости.
  //=============================================================================
  public void SetProperties(AISpeedControlProperties properties);
  
  //=============================================================================
  // Name: ResetProperties
  // Desc: Сбрасывает параметры контроля скорости для этой записи на параметры по
  //       умолчания для текущего игрового приоритета.
  //=============================================================================
  public void ResetProperties(void);
  
	//
	// РЕАЛИЗАЦИЯ
	//

  int _id = -1;                               //Идентификатор записи
  string _name;                               //Наименвоанеи записи
  int _priority;                              //Игровой приоритет
  AISpeedControlProperties _properties;       //Параметры контроля скорости

  public AICustomPriorityItem Init(int id, string name, int priority)
  {
    Str.TrimLeft(name, " ");
    Str.TrimRight(name, " ");
    if(_id >= 0) { Interface.Exception("Объект AICustomPriorityItem уже инициалищирован."); return null; }
    if(id < 0) { Interface.Exception("Аргумент id имеет отрицательное значение"); return null; }
    if(name == "") { Interface.Exception("Аргумент name не может быть пустым"); return null; }
    if(priority < 1 or priority > 3) { Interface.Exception("Недопустимое значение аргумента priority"); return null; }
    _id = id;
    _name = name;
    _priority = priority;
    if(priority == 3) _properties = AIStandartSpeedControlProperties.CreateShunt();
    else if(priority == 2) _properties = AIStandartSpeedControlProperties.CreateCargo();
    else if(priority == 1) _properties = AIStandartSpeedControlProperties.CreatePassenger();
    return me;
  }
  
  public int GetId(void) 
  { 
    return _id; 
  }
  
  public string GetName(void) 
  {
    return _name;
  }
  
  public void SetName(string name)
  {
    Str.TrimLeft(name, " ");
    Str.TrimRight(name, " ");
    if(name == "") { Interface.Exception("Аргумент name не может быть пустым"); return; }
    _name = name;
  }
  
  public int GetPriority(void)
  {
    return _priority;
  }
  
  public void SetPriority(int priority, bool resetProperties)
  {
    if(priority < 1 or priority > 3) { Interface.Exception("Недопустимое значение аргумента priority"); return; }
    _priority = priority;
    if(resetProperties) {
      if(priority == 3) _properties = AIStandartSpeedControlProperties.CreateShunt();
      else if(priority == 2) _properties = AIStandartSpeedControlProperties.CreateCargo();
      else if(priority == 1) _properties = AIStandartSpeedControlProperties.CreatePassenger();
    }
  }
  
  public AISpeedControlProperties GetProperties(void)
  {
    return _properties;
  }
  
  public void SetProperties(AISpeedControlProperties properties)
  {
    if(properties == null) { Interface.Exception("Значение аргумента properties не может быть null"); return; }
    _properties = properties; 
  }
  
  public void ResetProperties(void)
  {
    if(_priority == 3) _properties = AIStandartSpeedControlProperties.CreateShunt();
    else if(_priority == 2) _properties = AIStandartSpeedControlProperties.CreateCargo();
    else if(_priority == 1) _properties = AIStandartSpeedControlProperties.CreatePassenger();
  }

};                                             