//=============================================================================
// File: aistopmarkerbase.gs
// Desc: Базовый скрипт стоп маркера
// Auth: Алексей 'Эрендир' Зверев 2020 © Trainz Dev Team
//       Licence: GNU GPL
//=============================================================================

include "trackmark.gs"
include "aicore.gs"

//=============================================================================
// Name: AIPrioritySettingsBehavior
// Desc: Базовый класс стоп маркера
//=============================================================================
class AIStopMarkerBase isclass TrackMark
{

//=============================================================================
// Name: TYPE_*
// Desc: Набор констант, определяющих тип маркера
//=============================================================================
  public define int TYPE_NOSET          = 0;  //Тип марера не задан
  public define int TYPE_FIRSTWAGON     = 1;  //Остановка локомотива
  public define int TYPE_LOCOMOTIVE     = 2;  //Остановка локомотива
  public define int TYPE_CONSIST        = 3;  //Остановка состава

  //=============================================================================
  // Name: StationName
  // Desc: Наименование сатнции или остановочного пункта, с которым связан 
  //       маркер.
  // Retn: Строка, представляющая имя станции или значение null, если маркер
  //       не связан со станцией или остановочным пунктом.
  //=============================================================================
  public final string StationName(void);

  //=============================================================================
  // Name: Type
  // Desc: Тип маркера
  // Retn: Значение одной из констант TYPE_*, указывающее на тип маркера.
  //=============================================================================
  public final int Type(void);

  //=============================================================================
  // Name: SectionCount
  // Desc: Количество секций локомотива, связаное с некоторыми типами маркера.
  // Retn: Количество секций, или значение 0 — если маркер не определяет 
  //       количество секций и может быть выбран с любым параметром, или значение
  //       -1 — текущий тип маркера не поддерживает количество секций. 
  //=============================================================================
  public final int SectionCount(void);
  
  //=============================================================================
  // Name: SetStationName
  // Desc: Назначает названеи станции, к которому будет привязан маркер.
  // Parm: stationName — наименование станции или значение null, если маркер не
  //       должен быть привящан к станции.
  //=============================================================================
  final void SetStationName(string stationName);
  
  //=============================================================================
  // Name: SetType
  // Desc: Устанавливает тип маркера
  // Parm: type — одна из констант TYPE_*, определяющая тип марера.
  //=============================================================================
  final void SetType(int type);
  
  //=============================================================================
  // Name: SetSection
  // Desc: Устанавливает количество секций для некоторых типов марера.
  // Parm: type — значение больше 0 (ноля), определяющее количество секций (для
  //       TYPE_LOCOMOTIVE не более 4, для TYPE_CONSIST не более 8), или значение
  //       0 (ноль), если маркер соответсвует любому количеству секций.
  // Note: Попытка установки количества секций для типов маркера не 
  //       поддерживающих количество секций приведёт к вызову исключения.       
  //=============================================================================
  final void SetSection(int sectionCount);


	//
	// РЕАЛИЗАЦИЯ
	//
  
  AICore _core;
  string _stationname;
  int _type = TYPE_NOSET;
  int _sectioncount; 

  public final string StationName(void)
  {
    if (_stationname and _stationname.size()) return _stationname;
    return (string)null;
  }

  public final int Type(void)
  {
    return _type;
  }

  public final int SectionCount(void)
  {
    if (_type == TYPE_LOCOMOTIVE or _type == TYPE_CONSIST) return _sectioncount;
    return -1;
  }

  final void SetStationName(string stationName)
  {
    if (stationName) {
      Str.TrimLeft(stationName, " ");
      Str.TrimRight(stationName, " ");
      if (!stationName.size()) stationName = null; 
    }
    if (_stationname != stationName) {
      if (_stationname) _core.TryRemoveStationName(_stationname);
      if (stationName) _core.AddStationName(stationName);
      _stationname = stationName; 
    }
  }
  
  final void SetType(int type)
  {
    if (type < 0 or type > 3) { Exception("AIStopMarkerBase.SetType> Аргумент type имеет недопустимое значение."); return; }
    if (_type != type) {
      _type = type;
      _sectioncount = 0;
    }
  }

  final void SetSection(int sectionCount)
  {
    if (_type != TYPE_LOCOMOTIVE and _type != TYPE_CONSIST) { Exception("AIStopMarkerBase.SetSection> Недопустимо назначать количество секций для текущего типа маркера."); return; }
    if (sectionCount < 0 or (_type == TYPE_LOCOMOTIVE and sectionCount > 4) or sectionCount > 8) { Exception("AIStopMarkerBase.SetSection> Аргумент sectionCount имеет недопустимое значение."); return; }
    _sectioncount = sectionCount;
  }

  public mandatory Soup GetProperties(void)
  {
    Soup soup = inherited();
    if (_stationname) soup.SetNamedTag("StationName", _stationname);
    soup.SetNamedTag("Type", _type);
    soup.SetNamedTag("Section", _sectioncount);
    return soup;
  }

  public mandatory void SetProperties(Soup soup)
  {
    inherited(soup);
    _type = soup.GetNamedTagAsInt("Type", _type);
    _sectioncount = soup.GetNamedTagAsInt("Section", _sectioncount);
    SetStationName(soup.GetNamedTag("StationName"));
  }
  
  public mandatory void Init(Asset asset)
  {
    inherited(asset);
    _core = cast<AICore>TrainzScript.GetLibrary(asset.LookupKUIDTable("ai-driver-core"));
  } 

};