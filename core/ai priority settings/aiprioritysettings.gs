//=============================================================================
// File: aiprioritysettings.gs
// Desc: ���������� ������� ���������� ������������ 
// Auth: ������� '�������' ������ 2020 � Trainz Dev Team
//       Licence: GNU GPL
//=============================================================================

include "aiprioritysettingsbehavior.gs"

//=============================================================================
// Name: AIPrioritySettingsBehavior
// Desc: ���������� ������� ���������� ������������
//=============================================================================
final class AIPrioritySettings isclass AIPrioritySettingsBehavior
{

  StringTable _strtable;
  bool _selectcustom;
  int _selectindex = -1;

  AISpeedControlProperties[] _defaultproperties;


  final void CopyProprties(AISpeedControlProperties source, AISpeedControlProperties destination)
  {
    destination.StartThrottle = source.StartThrottle; 
    destination.AccFactor = source.AccFactor; 
    destination.StartAccFactor = source.StartAccFactor; 
    destination.AccFactorStep = source.AccFactorStep; 
    destination.AccFactorStepMass = source.AccFactorStepMass; 
    destination.AccelStep = source.AccelStep; 
    destination.DecelStep = source.DecelStep; 
    destination.BrakeFactor = source.BrakeFactor; 
    destination.FullStopDist = source.FullStopDist; 
    destination.ReducedSpeedDist = source.ReducedSpeedDist; 
    destination.ReducedSpeed = source.ReducedSpeed; 
    destination.CriticalSpeedFactor = source.CriticalSpeedFactor; 
    destination.PauseAfterFirstThrottle = source.PauseAfterFirstThrottle; 
  }

  final bool CompareProprties(AISpeedControlProperties one, AISpeedControlProperties two)
  {
    return one.StartThrottle == two.StartThrottle and one.AccFactor == two.AccFactor and one.StartAccFactor == two.StartAccFactor and one.AccFactorStep == two.AccFactorStep and  
    one.AccFactorStepMass == two.AccFactorStepMass and one.AccelStep == two.AccelStep and one.DecelStep == two.DecelStep and one.BrakeFactor == two.BrakeFactor and 
    one.FullStopDist == two.FullStopDist and one.ReducedSpeedDist == two.ReducedSpeedDist and one.ReducedSpeed == two.ReducedSpeed and one.CriticalSpeedFactor == two.CriticalSpeedFactor and 
    one.PauseAfterFirstThrottle == two.PauseAfterFirstThrottle; 
  }







  void BuildPropertyHTML(AISpeedControlProperties properties, HTMLBuffer buff)
  {
    buff.Print(HTMLWindow.StartRow());
      buff.Print(HTMLWindow.StartCell());
        buff.Print("<nowrap>");
          buff.Print(_strtable.GetString("properties-startthrottle") + ":");
        buff.Print("</nowrap>");
      buff.Print(HTMLWindow.EndCell());
      buff.Print(HTMLWindow.StartCell("width=100%"));
        buff.Print(properties.StartThrottle);
      buff.Print(HTMLWindow.EndCell());
    buff.Print(HTMLWindow.EndRow());

    buff.Print(HTMLWindow.StartRow());
      buff.Print(HTMLWindow.StartCell());
        buff.Print("<nowrap>");
          buff.Print(_strtable.GetString("properties-accfactor") + ":");
        buff.Print("</nowrap>");
      buff.Print(HTMLWindow.EndCell());
      buff.Print(HTMLWindow.StartCell("width=100%"));
        buff.Print(properties.AccFactor);
      buff.Print(HTMLWindow.EndCell());
    buff.Print(HTMLWindow.EndRow());

    buff.Print(HTMLWindow.StartRow());
      buff.Print(HTMLWindow.StartCell());
        buff.Print("<nowrap>");
          buff.Print(_strtable.GetString("properties-startaccfactor") + ":");
        buff.Print("</nowrap>");
      buff.Print(HTMLWindow.EndCell());
      buff.Print(HTMLWindow.StartCell("width=100%"));
        buff.Print(properties.StartAccFactor);
      buff.Print(HTMLWindow.EndCell());
    buff.Print(HTMLWindow.EndRow());

    buff.Print(HTMLWindow.StartRow());
      buff.Print(HTMLWindow.StartCell());
        buff.Print("<nowrap>");
          buff.Print(_strtable.GetString("properties-accfactorstep") + ":");
        buff.Print("</nowrap>");
      buff.Print(HTMLWindow.EndCell());
      buff.Print(HTMLWindow.StartCell("width=100%"));
        buff.Print(properties.AccFactorStep);
      buff.Print(HTMLWindow.EndCell());
    buff.Print(HTMLWindow.EndRow());

    buff.Print(HTMLWindow.StartRow());
      buff.Print(HTMLWindow.StartCell());
        buff.Print("<nowrap>");
          buff.Print(_strtable.GetString("properties-accfactorstepmass") + ":");
        buff.Print("</nowrap>");
      buff.Print(HTMLWindow.EndCell());
      buff.Print(HTMLWindow.StartCell("width=100%"));
        buff.Print(properties.AccFactorStepMass);
      buff.Print(HTMLWindow.EndCell());
    buff.Print(HTMLWindow.EndRow());

    buff.Print(HTMLWindow.StartRow());
      buff.Print(HTMLWindow.StartCell());
        buff.Print("<nowrap>");
          buff.Print(_strtable.GetString("properties-accelstep") + ":");
        buff.Print("</nowrap>");
      buff.Print(HTMLWindow.EndCell());
      buff.Print(HTMLWindow.StartCell("width=100%"));
        buff.Print(properties.AccelStep);
      buff.Print(HTMLWindow.EndCell());
    buff.Print(HTMLWindow.EndRow());

    buff.Print(HTMLWindow.StartRow());
      buff.Print(HTMLWindow.StartCell());
        buff.Print("<nowrap>");
          buff.Print(_strtable.GetString("properties-decelstep") + ":");
        buff.Print("</nowrap>");
      buff.Print(HTMLWindow.EndCell());
      buff.Print(HTMLWindow.StartCell("width=100%"));
        buff.Print(properties.DecelStep);
      buff.Print(HTMLWindow.EndCell());
    buff.Print(HTMLWindow.EndRow());

    buff.Print(HTMLWindow.StartRow());
      buff.Print(HTMLWindow.StartCell());
        buff.Print("<nowrap>");
          buff.Print(_strtable.GetString("properties-brakefactor") + ":");
        buff.Print("</nowrap>");
      buff.Print(HTMLWindow.EndCell());
      buff.Print(HTMLWindow.StartCell("width=100%"));
        buff.Print(properties.BrakeFactor);
      buff.Print(HTMLWindow.EndCell());
    buff.Print(HTMLWindow.EndRow());

    buff.Print(HTMLWindow.StartRow());
      buff.Print(HTMLWindow.StartCell());
        buff.Print("<nowrap>");
          buff.Print(_strtable.GetString("properties-reducedspeeddist") + ":");
        buff.Print("</nowrap>");
      buff.Print(HTMLWindow.EndCell());
      buff.Print(HTMLWindow.StartCell("width=100%"));
        buff.Print(properties.ReducedSpeedDist);
      buff.Print(HTMLWindow.EndCell());
    buff.Print(HTMLWindow.EndRow());

    buff.Print(HTMLWindow.StartRow());
      buff.Print(HTMLWindow.StartCell());
        buff.Print("<nowrap>");
          buff.Print(_strtable.GetString("properties-fullstopdist") + ":");
        buff.Print("</nowrap>");
      buff.Print(HTMLWindow.EndCell());
      buff.Print(HTMLWindow.StartCell("width=100%"));
        buff.Print(properties.FullStopDist);
      buff.Print(HTMLWindow.EndCell());
    buff.Print(HTMLWindow.EndRow());

    buff.Print(HTMLWindow.StartRow());
      buff.Print(HTMLWindow.StartCell());
        buff.Print("<nowrap>");
          buff.Print(_strtable.GetString("properties-reducedspeed") + ":");
        buff.Print("</nowrap>");
      buff.Print(HTMLWindow.EndCell());
      buff.Print(HTMLWindow.StartCell("width=100%"));
        buff.Print(properties.ReducedSpeed);
      buff.Print(HTMLWindow.EndCell());
    buff.Print(HTMLWindow.EndRow());

    buff.Print(HTMLWindow.StartRow());
      buff.Print(HTMLWindow.StartCell());
        buff.Print("<nowrap>");
          buff.Print(_strtable.GetString("properties-criticalspeedfactor") + ":");
        buff.Print("</nowrap>");
      buff.Print(HTMLWindow.EndCell());
      buff.Print(HTMLWindow.StartCell("width=100%"));
        buff.Print(properties.CriticalSpeedFactor);
      buff.Print(HTMLWindow.EndCell());
    buff.Print(HTMLWindow.EndRow());

    buff.Print(HTMLWindow.StartRow());
      buff.Print(HTMLWindow.StartCell());
        buff.Print("<nowrap>");
          buff.Print(_strtable.GetString("properties-pauseafterstart") + ":");
        buff.Print("</nowrap>");
      buff.Print(HTMLWindow.EndCell());
      buff.Print(HTMLWindow.StartCell("width=100%"));
        buff.Print(properties.PauseAfterFirstThrottle);
      buff.Print(HTMLWindow.EndCell());
    buff.Print(HTMLWindow.EndRow());
  }




  public string GetDescriptionHTML(void)           //      
  {
    bool clinpboardcontaindata = GetCore().ContainsDataInClipboard("AISpeedControlProperties");
    int i, count = CustomPriorityItemCount();
    HTMLBuffer buff = HTMLBufferStatic.Construct();
    buff.Print("<font size=10 color=#31859c><b>");
    buff.Print(_strtable.GetString("title"));
    buff.Print("</b></font><br><br>");
    if (!IsActive()) {
      buff.Print("<font size=6 color=#7c2d1b><b>");
      buff.Print(_strtable.GetString("warning-noactive"));
      buff.Print("</b></font>");
      return buff.AsString();
    }
    buff.Print(_strtable.GetString("priority-standart"));
    buff.Print("<br />");
    buff.Print(HTMLWindow.StartTable("width=100%"));
    for (i = 0; i <= 3; ++i) {
      AISpeedControlProperties properties = GetSpeedControlProperties(i);
      int priority = i;
      if (i == 0) priority = 1; 
      buff.Print(HTMLWindow.StartRow());
      buff.Print(HTMLWindow.StartCell("bgcolor=#828282"));
      buff.Print(HTMLWindow.StartTable("width=100%"));
      buff.Print(HTMLWindow.StartRow());
        buff.Print(HTMLWindow.StartCell());
          if (!_selectcustom and _selectindex == i) 
            buff.Print(HTMLWindow.MakeLink("live://property/select-standart/" + i, HTMLWindow.MakeImage("opened.png", false, 16, 16), _strtable.GetString("")));
          else buff.Print(HTMLWindow.MakeLink("live://property/select-standart/" + i, HTMLWindow.MakeImage("closed.png", false, 16, 16), _strtable.GetString("")));
        buff.Print(HTMLWindow.EndCell());
        buff.Print(HTMLWindow.MakeCell(_strtable.GetString("priority-standart-" + i), "width=100%"));
        buff.Print(HTMLWindow.StartCell());
          buff.Print("<nowrap>");
            buff.Print(_strtable.GetString1("priority-value", priority));
          buff.Print("</nowrap>");
        buff.Print(HTMLWindow.EndCell());
        buff.Print(HTMLWindow.MakeCell(HTMLWindow.MakeLink("live://property/copy-standart/" + i, HTMLWindow.MakeImage("copy.png", false, 16, 16), _strtable.GetString("tooltip-copy"))));
        buff.Print(HTMLWindow.StartCell());
          if (!clinpboardcontaindata) buff.Print(HTMLWindow.MakeImage("past-d.png", false, 16, 16));
          else buff.Print(HTMLWindow.MakeLink("live://property/past-standart/" + i, HTMLWindow.MakeImage("past.png", false, 16, 16), _strtable.GetString("tooltip-past")));
        buff.Print(HTMLWindow.EndCell());
        buff.Print(HTMLWindow.StartCell());
          if(CompareProprties(properties, _defaultproperties[i])) buff.Print(HTMLWindow.MakeImage("restore-d.png", false, 16, 16));
          else buff.Print(HTMLWindow.MakeLink("live://property/restore-standart/" + i, HTMLWindow.MakeImage("restore.png", false, 16, 16), _strtable.GetString("tooltip-restore")));
        buff.Print(HTMLWindow.EndCell());
      buff.Print(HTMLWindow.EndRow());
      if (!_selectcustom and _selectindex == i) {
        buff.Print(HTMLWindow.StartRow());
        buff.Print(HTMLWindow.StartCell("colspan=6"));
        buff.Print(HTMLWindow.StartTable("width=100%"));
        BuildPropertyHTML(properties, buff);
        buff.Print(HTMLWindow.EndTable());
        buff.Print(HTMLWindow.EndCell());
        buff.Print(HTMLWindow.EndRow());
      }
      buff.Print(HTMLWindow.EndTable());
      buff.Print(HTMLWindow.EndCell());
      buff.Print(HTMLWindow.EndRow());
    }
    buff.Print(HTMLWindow.EndTable());
    buff.Print("<br />");
    buff.Print(_strtable.GetString("priority-custom"));
    buff.Print("<br />");
    buff.Print(HTMLWindow.StartTable("width=100%"));
    for (i = 0; i < count; ++i) {
      AICustomPriorityItem customitem = CustomPriorityItem(i);
      buff.Print(HTMLWindow.StartRow());
      buff.Print(HTMLWindow.StartCell("bgcolor=#828282"));
      buff.Print(HTMLWindow.StartTable("width=100%"));
      buff.Print(HTMLWindow.StartRow());
        buff.Print(HTMLWindow.StartCell());
          if (_selectcustom and _selectindex == i) 
            buff.Print(HTMLWindow.MakeLink("live://property/select-custom/" + i, HTMLWindow.MakeImage("opened.png", false, 16, 16), _strtable.GetString("")));
          else buff.Print(HTMLWindow.MakeLink("live://property/select-custom/" + i, HTMLWindow.MakeImage("closed.png", false, 16, 16), _strtable.GetString("")));
        buff.Print(HTMLWindow.EndCell());
        buff.Print(HTMLWindow.MakeCell(HTMLWindow.MakeLink("live://property/rename-custom/" + i, customitem.GetName(), _strtable.GetString("tooltip-custom-rename")), "width=100%"));
        buff.Print(HTMLWindow.StartCell());
          buff.Print("<nowrap>");
            buff.Print(_strtable.GetString1("priority-value", HTMLWindow.MakeLink("live://property/change-priority-custom/" + i, customitem.GetPriority(), _strtable.GetString("tooltip-custom-changegamepriority"))));
          buff.Print("</nowrap>");
        buff.Print(HTMLWindow.EndCell());
        buff.Print(HTMLWindow.MakeCell(HTMLWindow.MakeLink("live://property/copy-custom/" + i, HTMLWindow.MakeImage("copy.png", false, 16, 16), _strtable.GetString("tooltip-copy"))));
        buff.Print(HTMLWindow.StartCell());
          if (!clinpboardcontaindata) buff.Print(HTMLWindow.MakeImage("past-d.png", false, 16, 16));
          else buff.Print(HTMLWindow.MakeLink("live://property/past-custom/" + i, HTMLWindow.MakeImage("past.png", false, 16, 16), _strtable.GetString("tooltip-past")));
        buff.Print(HTMLWindow.EndCell());
        buff.Print(HTMLWindow.StartCell());
          if (CompareProprties(customitem.GetProperties(), _defaultproperties[customitem.GetPriority()])) buff.Print(HTMLWindow.MakeImage("restore-d.png", false, 16, 16));
          else buff.Print(HTMLWindow.MakeLink("live://property/restore-custom/" + i, HTMLWindow.MakeImage("restore.png", false, 16, 16), _strtable.GetString("tooltip-restore")));
        buff.Print(HTMLWindow.EndCell());
        buff.Print(HTMLWindow.StartCell());
          if (i == count - 1) buff.Print(HTMLWindow.MakeImage("move-down-d.png", false, 16, 16));
          else buff.Print(HTMLWindow.MakeLink("live://property/movedown-custom/" + i, HTMLWindow.MakeImage("move-down.png", false, 16, 16), _strtable.GetString("tooltip-movedown")));
        buff.Print(HTMLWindow.EndCell());
        buff.Print(HTMLWindow.StartCell());
          if(i == 0) buff.Print(HTMLWindow.MakeImage("move-up-d.png", false, 16, 16));
          else buff.Print(HTMLWindow.MakeLink("live://property/moveup-custom/" + i, HTMLWindow.MakeImage("move-up.png", false, 16, 16), _strtable.GetString("tooltip-moveup")));
        buff.Print(HTMLWindow.EndCell());
        buff.Print(HTMLWindow.MakeCell(HTMLWindow.MakeLink("live://property/remove-custom/" + i, HTMLWindow.MakeImage("remove.png", false, 16, 16), _strtable.GetString("tooltip-remove"))));
      buff.Print(HTMLWindow.EndRow());
      if(_selectcustom and _selectindex == i) {
        buff.Print(HTMLWindow.StartRow());
        buff.Print(HTMLWindow.StartCell("colspan=9"));
        buff.Print(HTMLWindow.StartTable("width=100%"));
        BuildPropertyHTML(customitem.GetProperties(), buff);
        buff.Print(HTMLWindow.EndTable());
        buff.Print(HTMLWindow.EndCell());
        buff.Print(HTMLWindow.EndRow());
      }
      buff.Print(HTMLWindow.EndTable());
      buff.Print(HTMLWindow.EndCell());
      buff.Print(HTMLWindow.EndRow());
    }
    buff.Print(HTMLWindow.EndTable());
    buff.Print("<br />");
    if (count >= 10) buff.Print("<font color=#646466>" + _strtable.GetString("priority-add") + "</font>");
    else buff.Print(HTMLWindow.MakeLink("live://property/create-custom", _strtable.GetString("priority-add"), _strtable.GetString("tooltip-priority-add")));
    buff.Print("<br />");
    return buff.AsString();
  }

  string GetPropertyType(string propertyID)
  {
    if (propertyID[0, 15] == "select-standart" or propertyID[0, 13] == "select-custom" or propertyID[0, 22] == "change-priority-custom" or propertyID[0, 13] == "copy-standart" or
    propertyID[0, 13] == "past-standart" or propertyID[0, 16] == "restore-standart" or propertyID[0, 11] == "copy-custom" or propertyID[0, 11] == "past-custom" or
    propertyID[0, 14] == "restore-custom" or propertyID[0, 15] == "movedown-custom" or propertyID[0, 13] == "moveup-custom" or propertyID[0, 13] == "remove-custom") return "link";
    else if (propertyID == "create-custom" or propertyID[0, 13] == "rename-custom") return "string,1,20";
    
                                                                                 

    return inherited(propertyID);
  }

  string GetPropertyName(string propertyID)
  {
    if (propertyID == "create-custom") return _strtable.GetString("editvalue-newname");
    else if (propertyID[0,13] == "rename-custom") return _strtable.GetString("editvalue-editname");

    return inherited(propertyID);
  }

  string GetPropertyValue(string propertyID)
  {
    if (propertyID[0, 13] == "rename-custom") {
      int index = Str.ToInt(propertyID[14,]);
      if (index >= 0 and index < CustomPriorityItemCount())
        return CustomPriorityItem(index).GetName();        
        
        
        
        

    }
    return inherited(propertyID);
  }
    
  
  
  
  void LinkPropertyValue(string propertyID)
  {
    if (propertyID[0,15] == "select-standart") {
      int index = Str.ToInt(propertyID[16,]);
      if(index < 0 or index > 3 or (!_selectcustom and _selectindex == index)) index = -1;
      _selectcustom = false;
      _selectindex = index;
    } else if(propertyID[0,13] == "select-custom") {
      int index = Str.ToInt(propertyID[14,]);
      if(index < 0 or index > 3 or (_selectcustom and _selectindex == index)) index = -1;
      _selectcustom = true;
      _selectindex = index;
    } else if(propertyID[0, 22] == "change-priority-custom") {
      int index = Str.ToInt(propertyID[23,]);
      if (index >= 0 and index < CustomPriorityItemCount()) {
        int newpriority = CustomPriorityItem(index).GetPriority() + 1;
        if (newpriority > 3) newpriority = 1;  
        CustomPriorityItem(index).SetPriority(newpriority);      
      }
    } else if(propertyID[0,13] == "copy-standart") {
      int index = Str.ToInt(propertyID[14,]);
      if (index >= 0 and index <= 3)
        GetCore().SetClipboardData("AISpeedControlProperties", GetSpeedControlPropertiesSoup(GetSpeedControlProperties(index))); 
    } else if(propertyID[0,13] == "past-standart") {
      int index = Str.ToInt(propertyID[14,]);
      AICore core = GetCore();
      if (index >= 0 and index <= 3 and core.ContainsDataInClipboard("AISpeedControlProperties")) {
        Soup data = core.GetClipboardData("AISpeedControlProperties");
        if (data) {
          AISpeedControlProperties properties = GetSpeedControlProperties(index); 
          CopyProprties(RestoreSpeedControlProperties(data, properties), properties);
          _selectcustom = false;
          _selectindex = index;
        } 
      }
    } else if(propertyID[0,16] == "restore-standart") {
      int index = Str.ToInt(propertyID[17,]);
      if (index >= 0 and index <= 3) 
        CopyProprties(_defaultproperties[index], GetSpeedControlProperties(index));
    } else if(propertyID[0,11] == "copy-custom") {
      int index = Str.ToInt(propertyID[12,]);
      if (index >= 0 and index < CustomPriorityItemCount()) 
        GetCore().SetClipboardData("AISpeedControlProperties", GetSpeedControlPropertiesSoup(CustomPriorityItem(index).GetProperties())); 
    } else if(propertyID[0,11] == "past-custom") {
      int index = Str.ToInt(propertyID[12,]);
      if (index >= 0 and index < CustomPriorityItemCount() and GetCore().ContainsDataInClipboard("AISpeedControlProperties")) {
        Soup data = GetCore().GetClipboardData("AISpeedControlProperties");
        if (data) CustomPriorityItem(index).SetProperties(RestoreSpeedControlProperties(data, CustomPriorityItem(index).GetProperties()));
      } 
    } else if(propertyID[0,14] == "restore-custom") {
      int index = Str.ToInt(propertyID[15,]);
      if (index >= 0 and index < CustomPriorityItemCount()) 
        CopyProprties(_defaultproperties[CustomPriorityItem(index).GetPriority()], CustomPriorityItem(index).GetProperties());
    } else if(propertyID[0,15] == "movedown-custom") {
      int index = Str.ToInt(propertyID[16,]);
      if (index >= 0 and index < CustomPriorityItemCount() - 1) {
        SwapCustomPriorityItems(index, index + 1);    
        if (_selectcustom and _selectindex == index) _selectindex = index + 1; 
        else if (_selectcustom and _selectindex == index + 1) _selectindex = index; 
      } 
    } else if(propertyID[0, 13] == "moveup-custom") {
      int index = Str.ToInt(propertyID[14,]);
      if (index > 0 and index < CustomPriorityItemCount()) {
        SwapCustomPriorityItems(index, index - 1);    
        if (_selectcustom and _selectindex == index) _selectindex = index - 1; 
        else if (_selectcustom and _selectindex == index - 1) _selectindex = index; 
      } 
    } else if(propertyID[0, 13] == "remove-custom") {
      int index = Str.ToInt(propertyID[14,]);
      if (index >= 0 and index < CustomPriorityItemCount()) {
        RemoveCustomPriorityItemAt(index); 
        if (_selectcustom and _selectindex == index) _selectindex = -1; 
      }
    } else inherited(propertyID);
  }
  
  
  void SetPropertyValue(string propertyID, string value)
  {
    if (propertyID == "create-custom") {
      Str.TrimLeft(value, " ");
      Str.TrimRight(value, " ");
      if (value.size()) {
        CreateCustomPriorityItem(value, 2);
        _selectcustom = true;
        _selectindex = CustomPriorityItemCount() - 1;
      }
      
    } else if(propertyID[0,13] == "rename-custom") {
      int index = Str.ToInt(propertyID[14,]);
      Str.TrimLeft(value, " ");
      Str.TrimRight(value, " ");
      if (value.size() and index >= 0 and index < CustomPriorityItemCount()) {
        CustomPriorityItem(index).SetName(value);      
      }
      
      
      
      
    } else inherited(propertyID, value);
  }


















  
  

  public mandatory void Init(Asset asset)
  {
    inherited(asset);
    _strtable = asset.GetStringTable();

    _defaultproperties = new AISpeedControlProperties[4];
    _defaultproperties[0] = AIStandartSpeedControlProperties.CreateMotorcCar();
    _defaultproperties[1] = AIStandartSpeedControlProperties.CreatePassenger();
    _defaultproperties[2] = AIStandartSpeedControlProperties.CreateCargo();
    _defaultproperties[3] = AIStandartSpeedControlProperties.CreateShunt();
    
  }


};