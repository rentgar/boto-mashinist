include "aisetpriorityschedulecommand.gs"
include "aiprioritysettingsbehavior.gs"
include "drivercommand.gs"


final class AISetPriorityCommand isclass DriverCommand
{
  
  AICore _core;

  StringTable _strtable;
  Menu _menu;

  public void AddCommandMenuItem(DriverCharacter driver, Menu menu)
  {
    menu.AddSubmenu(_strtable.GetString("menu-setpriority"), _menu);
  }
  
  DriverScheduleCommand CreateScheduleCommand(DriverCharacter driver, Soup soup)
	{
		AISetPriorityScheduleCommand cmd = new AISetPriorityScheduleCommand();
		cmd.Init(driver, me);
		cmd.SetProperties(soup);
    return cast<DriverScheduleCommand>cmd;
	}  

  final void CreateMenu(Message msg) 
  {
    Menu menu = Constructors.NewMenu();
    int i, count;
    for (i = 0; i <= 3; ++i) 
      menu.AddItem(_strtable.GetString("menu-priority-" + i), me, "AIDriverCommand", "Priority:" + i);
    AIPrioritySettingsBehavior prioritystore = _core.GetPrioritySettingsBehavior();
    if (prioritystore) {
      count = prioritystore.CustomPriorityItemCount(); 
      if (count) {
        menu.AddSeperator();
        for (i = 0; i < count; ++i) {
          AICustomPriorityItem item = prioritystore.CustomPriorityItem(i);
          menu.AddItem("[" + _strtable.GetString("prioritycode-" + item.GetPriority()) + "] " + item.GetName(), me, "AIDriverCommand", "Custom:" + item.GetId());
        }
      }
    }
    _menu = menu;
  }

  final void MenuHandler(Message msg)
  {
    string[] args = Str.Tokens(msg.minor, ":");
		DriverCharacter driver = cast<DriverCharacter>(msg.src);
	  DriverCommands commands = me.GetDriverCommands(msg);
  	Soup soup = Constructors.NewSoup();
    soup.SetNamedTag("Custom", args[0] == "Custom");
    soup.SetNamedTag("Id", Str.ToInt(args[1]));
		commands.AddDriverScheduleCommand(CreateScheduleCommand(driver, soup));
  }

  public void Init(Asset asset)
  {
		inherited(asset); 
    _core = cast<AICore>TrainzScript.GetLibrary(asset.LookupKUIDTable("ai-driver-core"));
		_strtable = asset.GetStringTable();
    Sniff(_core, "AIDriver", "PriorityChanged", true);
    AddHandler(me, "AIDriver", "PriorityChanged", "CreateMenu");
		AddHandler(me, "AIDriverCommand", "", "MenuHandler");
    CreateMenu(null);
  }

};