include "aiprioritysettingsbehavior.gs"
include "driverschedulecommand.gs"
include "aidriverbase.gs"

final class AISetPriorityCustomCommand isclass CustomCommand
{
  AICore _core;
  bool _iscustom;
  int _priorityid;
  
  public AISetPriorityCustomCommand Init(AICore core, bool isCustom, int priorityId)
  {
    _core = core;
    _iscustom = isCustom;
    _priorityid = priorityId;
    return me;
  }

  public bool Execute(Train train, int px, int py, int pz)
  {
    AITrainProperties trainproperties = cast<AITrainProperties>train.extension;
    if (!trainproperties) {
      trainproperties = new AITrainProperties(); 
      train.extension = trainproperties;     
    }
    trainproperties.priority = AITrainProperties.PRIORITY_STANDART; 
    if (_iscustom) {
      AIPrioritySettingsBehavior prioritystore = _core.GetPrioritySettingsBehavior();
      if (prioritystore) {
        AICustomPriorityItem item = prioritystore.FindCustomPriority(_priorityid);
        if (item) {
          train.SetTrainPriorityNumber(item.GetPriority());
          trainproperties.priority = AITrainProperties.PRIORITY_CUSTOM;
          trainproperties.priorityid = _priorityid; 
          return true;
        }
      }
      return false;
    } else {
      if (_priorityid < 0 or _priorityid > 3) train.SetTrainPriorityNumber(2);
      else if (_priorityid == 0){
        train.SetTrainPriorityNumber(1);
        trainproperties.priority = AITrainProperties.PRIORITY_MOTORCAR;
      } 
      else train.SetTrainPriorityNumber(_priorityid);
    }
    return true;
  }

  public bool ShouldStopTrainOnCompletion() { return false; }

};

final class AISetPriorityScheduleCommand isclass DriverScheduleCommand
{
  bool _iscustom;
  int _priorityid;
  
  public bool BeginExecute(DriverCharacter driver)
	{
		Train train = driver.GetTrain();
		if (!train) return false;
    AISetPriorityCustomCommand command = new AISetPriorityCustomCommand().Init(cast<AICore>TrainzScript.GetLibrary(GetAsset().LookupKUIDTable("ai-driver-core")), _iscustom, _priorityid);
		driver.DriverCustomCommand(command);
		driver.DriverIssueSchedule();
		return true;
	}

	public object GetIcon(void)
  {
    return (object)(GetDriverCommand());
  }

  public string GetTooltip(void) 
  {
		StringTable strtable = GetAsset().GetStringTable();
    if (_iscustom) {
      AICore core = cast<AICore>TrainzScript.GetLibrary(GetAsset().LookupKUIDTable("ai-driver-core"));
      AIPrioritySettingsBehavior prioritystore = core.GetPrioritySettingsBehavior();
      if (prioritystore) {
        AICustomPriorityItem item = prioritystore.FindCustomPriority(_priorityid);
        if (item) return strtable.GetString1("tooltip-setpriority-custom", "[" + strtable.GetString("prioritycode-" + item.GetPriority()) + "] " + item.GetName());
      }
      return strtable.GetString("tooltip-setpriority-unknown");
    }
    return strtable.GetString1("tooltip-setpriority", strtable.GetString("priority-" + _priorityid));
  }

  public Soup GetProperties(void) 
  { 
    Soup soup = inherited();
    soup.SetNamedTag("Custom", _iscustom);
    soup.SetNamedTag("Id", _priorityid);
    return soup; 
  }

  public void SetProperties(Soup soup) 
  {
    _iscustom = soup.GetNamedTagAsBool("Custom");
    _priorityid = soup.GetNamedTagAsInt("Id");
    if (!_iscustom and (_priorityid < 0 or _priorityid > 3)) _priorityid = 2;
  }

};