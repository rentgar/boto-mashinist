//=============================================================================
// File: aistopmarker.gs
// Desc: ���������� ����-�������
// Auth: ������� '�������' ������ 2020 � Trainz Dev Team
//       Licence: GNU GPL
//=============================================================================

include "aistopmarkerbase.gs"

//=============================================================================
// Name: AIStopMarker
// Desc: �����, ����������� ����-������
//=============================================================================
final class AIStopMarker isclass AIStopMarkerBase
{

  StringTable _strtable;

  public final string MakeRadioButton(string linkId, bool checked, string tooltip) 
  {
    KUID imagekuid;
    if (checked) imagekuid = Constructors.GetTrainzAsset().LookupKUIDTable("radiobutton-on");
    else imagekuid = Constructors.GetTrainzAsset().LookupKUIDTable("radiobutton-off");
    string linkvalue = "<img kuid='" + imagekuid.GetHTMLString() + "' width=16 height=16>"; 
    return HTMLWindow.MakeLink("live://property/" + linkId, linkvalue, tooltip);
  }



  public string GetDescriptionHTML(void)                 
  {
    HTMLBuffer buffer = HTMLBufferStatic.Construct();
    buffer.Print("<font size=10 color=#31859c><b>");
    buffer.Print(_strtable.GetString("title"));
    buffer.Print("</b></font><br><br>");

    int i;  
    string stationname = _stationname;
    string sectioncount = SectionCount();
    if (!stationname) stationname = "";
    if (sectioncount == "0") sectioncount = _strtable.GetString("any"); 
    
    buffer.Print(HTMLWindow.StartTable("border=0"));
      buffer.Print(HTMLWindow.StartRow());
        buffer.Print(HTMLWindow.MakeCell(HTMLWindow.MakeBold(_strtable.GetString("settings-general")), "colspan=4"));
      buffer.Print(HTMLWindow.EndRow());
      buffer.Print(HTMLWindow.StartRow());
        buffer.Print(HTMLWindow.MakeSpacerCell(10));
        buffer.Print(HTMLWindow.MakeCell(_strtable.GetString("stationname") + ":"));
        buffer.Print(HTMLWindow.MakeCell(stationname)); 
        buffer.Print(HTMLWindow.StartCell());
          buffer.Print("[<font color=#8dd28a>" + HTMLWindow.MakeLink("live://property/newstation", "+", _strtable.GetString("tooltip-newstation")) + "</font>]&nbsp;");
          buffer.Print("[<font color=#7ac1ff>" + HTMLWindow.MakeLink("live://property/selectstation", "...", _strtable.GetString("tooltip-selectstation")) + "</font>]&nbsp;");
          if (!stationname.size()) buffer.Print("[X]");
          else buffer.Print("[<font color=#f38b76>" + HTMLWindow.MakeLink("live://property/resetstation", "X", _strtable.GetString("tooltip-resetstation")) + "</font>]");
        buffer.Print(HTMLWindow.EndCell());
      buffer.Print(HTMLWindow.EndRow());
      buffer.Print(HTMLWindow.EndRow());
        buffer.Print(HTMLWindow.MakeCell("�", "colspan=4")); 
      buffer.Print(HTMLWindow.EndRow());
      buffer.Print(HTMLWindow.StartRow());
        buffer.Print(HTMLWindow.MakeCell(HTMLWindow.MakeBold(_strtable.GetString("settings-type")), "colspan=4"));
      buffer.Print(HTMLWindow.EndRow());
      for (i = 0; i <= 3; ++i) {
        buffer.Print(HTMLWindow.StartRow());
          buffer.Print(HTMLWindow.MakeSpacerCell(10));
          buffer.Print(HTMLWindow.MakeCell(_strtable.GetString("type-" + i) + ":"));
          buffer.Print(HTMLWindow.MakeCell(MakeRadioButton("settype." + i, _type == i, _strtable.GetString("tooltip-type-" + i)))); 
        buffer.Print(HTMLWindow.EndRow());
      }
      if (_type == TYPE_LOCOMOTIVE or _type == TYPE_CONSIST) {
        buffer.Print(HTMLWindow.EndRow());
          buffer.Print(HTMLWindow.MakeCell("�", "colspan=4")); 
        buffer.Print(HTMLWindow.EndRow());
        buffer.Print(HTMLWindow.StartRow());
          buffer.Print(HTMLWindow.MakeCell(HTMLWindow.MakeBold(_strtable.GetString("settings-section")), "colspan=4"));
        buffer.Print(HTMLWindow.EndRow());
        buffer.Print(HTMLWindow.StartRow());
          buffer.Print(HTMLWindow.MakeSpacerCell(10));
          buffer.Print(HTMLWindow.MakeCell(_strtable.GetString("sectioncount") + ":"));
          buffer.Print(HTMLWindow.MakeCell(sectioncount)); 
          buffer.Print(HTMLWindow.StartCell());
            buffer.Print("[<font color=#d8ac6b>" + HTMLWindow.MakeLink("live://property/selectsection", "...", _strtable.GetString("tooltip-setsection")) + "</font>]&nbsp;");
            if (SectionCount() == 0) buffer.Print("[X]");
            else buffer.Print("[<font color=#7ac1ff>" + HTMLWindow.MakeLink("live://property/resetsection", "X", _strtable.GetString("tooltip-resetsection")) + "</font>]");
          buffer.Print(HTMLWindow.EndCell());
        buffer.Print(HTMLWindow.EndRow());
      }
    buffer.Print(HTMLWindow.EndTable());
    return buffer.AsString();
  }

  string GetPropertyType(string propertyID)
  {
    if (propertyID == "newstation") return "string,1,50";
    else if (propertyID == "selectstation") return "list,1";
    else if (propertyID == "selectsection" and _type == TYPE_CONSIST) return "int,1,8";
    else if (propertyID == "selectsection" and _type == TYPE_LOCOMOTIVE) return "int,1,4";
    else if (propertyID == "resetstation" or propertyID == "resetsection" or propertyID[0, 7] == "settype") return "link";
    return inherited(propertyID);
  }

  string GetPropertyName(string propertyID)
  {
    if (propertyID == "newstation") return _strtable.GetString("editvalue-newstation");
    else if (propertyID == "selectstation") return _strtable.GetString("editvalue-selectstation");
    else if (propertyID == "selectsection" and _type == TYPE_CONSIST) return _strtable.GetString("editvalue-selectsection-consist");
    else if (propertyID == "selectsection" and _type == TYPE_LOCOMOTIVE) return _strtable.GetString("editvalue-selectsection-firstloco");
    return inherited(propertyID);
  }

  string GetPropertyValue(string propertyID)
  {
    if (propertyID == "selectstation") return StationName();
    else if (propertyID == "selectsection" and SectionCount() > 0) return (string)SectionCount();
    return inherited(propertyID);
  }
  
  string[] GetPropertyElementList(string propertyID)
  {
    if (propertyID == "selectstation") return _core.StationNameList();
    return inherited(propertyID);
  }
  
  void SetPropertyValue(string propertyID, string value)
  {
    if (propertyID == "newstation") SetStationName(value);
    else inherited(propertyID, value);
  }

  void SetPropertyValue(string propertyID, string value, int index)
  {
    if (propertyID == "selectstation") SetStationName(value);
    else inherited(propertyID, value, index);
  }
  
  void SetPropertyValue(string propertyID, int value)
  {
    if (propertyID == "selectsection" and value > 0 and 
    ((_type == TYPE_CONSIST and value <= 8) or value <= 4)) SetSection(value);
    else inherited(propertyID, value);
  }
  
  void LinkPropertyValue(string propertyID)
  {
    if (propertyID[0, 7] == "settype") SetType(Str.ToInt(propertyID[8,]));
    else if (propertyID == "resetstation") SetStationName(null);
    else if (propertyID == "resetsection") SetSection(0);
  }    


  public void SetProperties(Soup soup)
  {
    inherited(soup);
    int i;
    for (i = 0; i <= 3; ++i)
      SetMeshVisible("type-" + i, _type == i, 0.0);
  }


  public void Init(Asset asset)
  {
    inherited(asset);
    _strtable = asset.GetStringTable();
  }

};